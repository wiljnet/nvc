FROM node:16-buster as builder

RUN export DEBIAN_FRONTEND='noninteractive' \
    && apt-get update \
    && apt-get install -y \
        python3 \
    && rm -rf /var/lib/apt/lists/*

RUN npm install -g typescript @ionic/cli

COPY ./app /app
copy ./site.env /app/

WORKDIR /app/

RUN npm install

RUN set -a && . /app/site.env && npm run build

FROM node:16-buster as app

RUN npm install -g serve

COPY --from=builder /app/build/ /app/build/

WORKDIR /app/


ENTRYPOINT ["serve", "-s", "build"]
