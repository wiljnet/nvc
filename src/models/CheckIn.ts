import { types } from "mobx-state-tree";
import { debug } from "../components/log";
import { pleasantFeelingNames }  from "./Data"
const Need = types.model({
  id: types.identifier,
  value: types.number,
});

export const CheckInModel = types
  .model({
    feelings: types.array(types.string),
    needs: types.map(Need),
  })
  .actions((self) => ({
    setFeeling: (feeling: string, value: boolean) => {
      if (value) {
        if (!self.feelings.includes(feeling)) {
          self.feelings.push(feeling);
        }
      } else {
        self.feelings.remove(feeling)
      }
    },
    setNeed: (need: string, value: number) => {
      self.needs.put({id: need, value})
    },
    clear: () => {
      self.feelings.clear()
      self.needs.clear()
    }
  }))
  .views(self => ({
    get metNeeds() {
      return Array.from(self.needs.values())
        .filter(x => x.value > 0)
        .map(x => x.id)
    },
    get unmetNeeds() {
      return Array.from(self.needs.values())
        .filter(x => x.value < 0)
        .map(x => x.id)
    },
    get pleasantFeelings() {
      return Array.from(self.feelings.values())
        .filter(x => pleasantFeelingNames.includes(x))
    },
    get unpleasantFeelings() {
      return Array.from(self.feelings.values())
        .filter(x => !pleasantFeelingNames.includes(x))
    },
    

  }));
