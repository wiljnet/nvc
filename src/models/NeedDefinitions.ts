export const NeedDefinitions = {
  abundance: `
  1. abundance, copiousness, teemingness 
    -- the property of a more than adequate quantity or supply
    -- "With food in abundance, there was more than they could eat."
`,
  acceptance: `   
  1. credence, acceptance -- (the mental attitude that something is believable and should be accepted as true; "he gave credence to the gossip"; "acceptance of Newtonian mechanics was unquestioned for 200 years")
  2. adoption, acceptance, acceptation, espousal -- (the act of accepting with approval; favorable reception; "its adoption by society"; "the proposal found wide acceptance")
  3. acceptance -- (the state of being acceptable and accepted; "torn jeans received no acceptance at the country club")

`,
  achievement: `   
  1. accomplishment, achievement -- (the action of accomplishing something)
`,
  acknowledgment: ` 
  1. recognition, acknowledgment, acknowledgement -- (the state or quality of being recognized or acknowledged; "the partners were delighted with the recognition of their work"; "she seems to avoid much in the way of recognition or acknowledgement of feminist work prior to her own")
  2. citation, cite, acknowledgment, credit, reference, mention, quotation -- (a short note recognizing a source of information or of a quoted passage; "the student's essay failed to list several important citations"; "the acknowledgments are usually printed at the front of a book"; "the article includes mention of similar clinical cases")
  3. acknowledgment, acknowledgement -- (a statement acknowledging something or someone; "she must have seen him but she gave no sign of acknowledgment"; "the preface contained an acknowledgment of those who had helped her")
`,
  adventure: ` 
  1. adventure, escapade, risky venture, dangerous undertaking -- (a wild and exciting undertaking (not necessarily lawful))
`,
  affection: ` 
  1. affection, affectionateness, fondness, tenderness, heart, warmness, warmheartedness, philia -- (a positive feeling of liking; "he had trouble expressing the affection he felt"; "the child won everyone's heart"; "the warmness of his welcome made us feel right at home")
`,
  air: `  
  1. air -- (a mixture of gases (especially oxygen) required for breathing; the stuff that the wind consists of; "air pollution"; "a smell of chemicals in the air"; "open a window and let in some air"; "I need some fresh air")
  2. air -- (the region above the ground; "her hand stopped in mid air"; "he threw the ball into the air")
  3. air, aura, atmosphere -- (a distinctive but intangible quality surrounding a person or thing; "an air of mystery"; "the house had a neglected air"; "an atmosphere of defeat pervaded the candidate's headquarters"; "the place had an aura of romance")
`,
  aliveness: `  
  1. animation, life, living, aliveness -- (the condition of living or the state of being alive; "while there's life there's hope"; "life depends on many chemical and physical processes")
  2. animateness, aliveness, liveness -- (the property of being animated; having animal life as distinguished from plant life)
`,
  appreciation: ` 
  1. appreciation, grasp, hold -- (understanding of the nature or meaning or quality or magnitude of something; "he has a good grasp of accounting practices")
  2. taste, appreciation, discernment, perceptiveness -- (delicate discrimination (especially of aesthetic values); "arrogance and lack of taste contributed to his rapid success"; "to ask at that particular time was the ultimate in bad taste")
  3. appreciation -- (an expression of gratitude; "he expressed his appreciation in a short note")
`,
  authenticity: `  
  1. authenticity, genuineness, legitimacy -- (undisputed credibility)
`,
  autonomy: ` 
  1. autonomy, liberty -- (immunity from arbitrary exercise of authority: political independence)
  2. autonomy, self-direction, self-reliance, self-sufficiency -- (personal independence)
`,
  awareness: `
  1. awareness, consciousness, cognizance, cognisance, knowingness -- (having knowledge of; "he had no awareness of his mistakes"; "his sudden consciousness of the problem he faced"; "their intelligence and general knowingness was impressive")
  2. awareness, sentience -- (state of elementary or undifferentiated consciousness; "the crash intruded on his awareness")
`,
  balance: ` 
  1. balance -- (a state of equilibrium)
  2. balance -- (equality between the totals of the credit and debit sides of an account)
  3. proportion, proportionality, balance -- (harmonious arrangement or relation of parts or elements within a whole (as in a design))

`,
  beauty: `   
  1. beauty -- (the qualities that give pleasure to the senses)
  2. smasher, stunner, knockout, beauty, ravisher, sweetheart, peach, lulu, looker, mantrap, dish -- (a very attractive or seductive looking woman)
  3. beauty, beaut -- (an outstanding example of its kind; "his roses were beauties"; "when I make a mistake it's a beaut")
`,
  "being seen": ``,
  belonging: `   
  1. belonging -- (happiness felt in a secure relationship; "with his classmates he felt a sense of belonging")
`,
  calm: ` 
  1. composure, calm, calmness, equanimity -- (steadiness of mind under stress; "he accepted their problems with composure and she with equanimity")
  2. calm air, calm -- (wind moving at less than 1 knot; 0 on the Beaufort scale)
`,
  Care: ` 
  1. care, attention, aid, tending -- (the work of providing treatment for or attending to someone or something; "no medical care was required"; "the old car needs constant attention")
  2. caution, precaution, care, forethought -- (judiciousness in avoiding harm or danger; "he exercised caution in opening the door"; "he handled the vase with care")
  3. concern, care, fear -- (an anxious feeling; "care had aged him"; "they hushed it up out of fear of public reaction")

`,
  celebration: `  
  1. celebration, jubilation -- (a joyful occasion for special festivities to mark some happy event)
  2. celebration, festivity -- (any joyous diversion)
  3. celebration, solemnization, solemnisation -- (the public performance of a sacrament or solemn ceremony with all appropriate ritual; "the celebration of marriage")
`,
  challenge: `
  1. challenge -- (a demanding or stimulating situation; "they reacted irrationally to the challenge of Russian power")
  2. challenge -- (a call to engage in a contest or fight)
  3. challenge -- (questioning a statement and demanding an explanation; "his challenge of the assumption that Japan is still our enemy")

`,
  choice: `   
  1. choice, pick, selection -- (the person or thing chosen or selected; "he was my pick for mayor")
  2. choice, selection, option, pick -- (the act of choosing or selecting; "your choice of colors was unfortunate"; "you can take your pick")
  3. option, alternative, choice -- (one of a number of things from which only one can be chosen; "what option did I have?"; "there no other alternative"; "my only choice is to refuse")
`,
  clarity: `  
  1. clarity, lucidity, lucidness, pellucidity, clearness, limpidity -- (free from obscurity and easy to understand; the comprehensibility of clear expression)
  2. clearness, clarity, uncloudedness -- (the quality of clear water; "when she awoke the clarity was back in her eyes")
`,
  closeness: `  
  1. closeness, intimacy -- (a feeling of being intimate and belonging together; "their closeness grew as the night wore on")
  2. stuffiness, closeness -- (the quality of being close and poorly ventilated)
  3. nearness, closeness -- (the spatial property resulting from a relatively small distance; "the sudden closeness of the dock sent him into action")

`,
  collaboration: `
  1. collaboration, coaction -- (act of working jointly; "they worked either in collaboration or independently")
  2. collaboration, collaborationism, quislingism -- (act of cooperating traitorously with an enemy that is occupying your country)
`,
  comfort: `  
  1. comfort, comfortableness -- (a state of being relaxed and feeling no pain; "he is a man who enjoys his comfort"; "she longed for the comfortableness of her armchair")
  2. comfort -- (a feeling of freedom from worry or disappointment)
  3. consolation, comfort, solace -- (the act of consoling; giving relief in affliction; "his presence was a consolation to her")

`,
  commitment: `   
  1. committedness, commitment -- (the trait of sincere and steadfast fixity of purpose; "a man of energy and commitment")
  2. commitment, allegiance, loyalty, dedication -- (the act of binding yourself (intellectually or emotionally) to a course of action; "his long commitment to public service"; "they felt no loyalty to a losing team")
  3. commitment -- (an engagement by contract involving financial obligation; "his business commitments took him to London")

`,
  communication: `
  1. communication, communicating -- (the activity of communicating; the activity of conveying information; "they could not act without official communication from Moscow")
  2. communication -- (something that is communicated by or to or between people or groups)
  3. communication -- (a connection allowing access between persons or places; "how many lines of communication can there be among four people?"; "a secret passageway provided communication between the two rooms")
`,
  Community: `
  1. community -- (a group of people living in a particular local area; "the team is drawn from all parts of the community")
  2. community -- (common ownership; "they shared a community of possessions")
  3. community -- (a group of nations having common interests; "they hoped to join the NATO community")

`,
  companionship: ` 
  1. company, companionship, fellowship, society -- (the state of being with someone; "he missed their company"; "he enjoyed the society of his friends")
`,
  compassion: `   
  1. compassion, compassionateness -- (a deep awareness of and sympathy for another's suffering)
  2. compassion, pity -- (the humane quality of understanding the suffering of others and wanting to do something about it)
`,
  confidence: `   
  1. assurance, self-assurance, confidence, self-confidence, authority, sureness -- (freedom from doubt; belief in yourself and your abilities; "his assurance in his superiority did not make him popular"; "after that failure he lost his confidence"; "she spoke with authority")
  2. confidence -- (a feeling of trust (in someone or something); "I have confidence in our team"; "confidence is always borrowed, never owned")
  3. confidence -- (a state of confident hopefulness that events will be favorable; "public confidence in the economy")

`,
  congruence: `  
  1. congruity, congruousness, congruence -- (the quality of agreeing; being suitable and appropriate)
`,
  Connection: `   
  1. connection, connexion, connectedness -- (a relation between things or events (as in the case of one causing the other or sharing features with it); "there was a connection between eating that pickle and having that nightmare")
  2. connection, link, connectedness -- (the state of being connected; "the connection between church and state is inescapable")
  3. connection, connexion, connector, connecter, connective -- (an instrumentality that connects; "he soldered the connection"; "he didn't have the right connector between the amplifier and the speakers")

`,
  consideration: `
  1. consideration -- (the process of giving careful thought to something)
  2. circumstance, condition, consideration -- (information that should be kept in mind when making a decision; "another consideration is the time it would take")
  3. consideration -- (a discussion of a topic (as in a meeting); "consideration of the traffic problem took more than an hour")

`,
  consistency: `  
  1. consistency, consistence, substance, body -- (the property of holding together and retaining its shape; "wool has more body than rayon"; "when the dough has enough consistency it is ready to bake")
  2. consistency, consistence -- (a harmonious uniformity or agreement among things or parts)
  3. consistency -- (logical coherence and accordance with the facts; "a rambling argument that lacked any consistency")

`,
  contribution: ` 
  1. contribution, part, share -- (the part played by a person in bringing about a result; "I am proud of my contribution in advancing the project"; "they all did their share of the work")
  2. contribution, donation -- (a voluntary gift (as of money or service or ideas) made to some worthwhile cause)
  3. contribution, donation -- (act of giving in common with others for a common purpose especially to a charity)

`,
  cooperation: `  
  1. cooperation -- (joint operation or action; "their cooperation with us was essential for the success of our mission")
  2. cooperation -- (the practice of cooperating; "economic cooperation"; "they agreed on a policy of cooperation")
`,
  courage: `   
  1. courage, courageousness, bravery, braveness -- (a quality of spirit that enables you to face danger or pain without showing fear)
`,
  creativity: `
  1. creativity, creativeness, creative thinking -- (the ability to create)
`,
  dependability: `   
  1. dependability, dependableness, reliability, reliableness -- (the quality of being dependable or reliable)
`,
  dignity: `  
  1. dignity, self-respect, self-regard, self-worth -- (the quality of being worthy of esteem or respect; "it was beneath his dignity to cheat"; "showed his true dignity when under pressure")
  2. dignity, lordliness, gravitas -- (formality in bearing and appearance; "he behaved with great dignity")
  3. dignity -- (high office or rank or station; "he respected the dignity of the emissaries")
`,
  discipline: `   
  1. discipline, subject, subject area, subject field, field, field of study, study, bailiwick -- (a branch of knowledge; "in what discipline is his doctorate?"; "teachers should be well trained in their subject"; "anthropology is the study of human beings")
  2. discipline -- (a system of rules of conduct or method of practice; "he quickly learned the discipline of prison routine"; "for such a plan to work requires discipline";)
  3. discipline -- (the trait of being well behaved; "he insisted on discipline among the troops")

`,
  discovery: `
  1. discovery, find, uncovering -- (the act of discovering something)
  2. discovery -- (something that is discovered)
  3. discovery, breakthrough, find -- (a productive insight)

`,
  ease: ` 
  1. ease, easiness, simplicity, simpleness -- (freedom from difficulty or hardship or effort; "he rose through the ranks with apparent ease"; "they put it into containers for ease of transportation"; "the very easiness of the deed held her back")
  2. ease, comfort -- (a freedom from financial difficulty that promotes a comfortable state; "a life of luxury and ease"; "he had all the material comforts of this world")
  3. relief, ease -- (the condition of being comfortable or relieved (especially after being relieved of distress); "he enjoyed his relief from responsibility"; "getting it off his conscience gave him some ease")
`,
  effectiveness: `
  1. effectiveness, effectivity, effectualness, effectuality -- (power to be effective; the quality of being able to bring about an effect)
  2. potency, effectiveness, strength -- (capacity to produce strong physiological or chemical effects; "the toxin's potency"; "the strength of the drinks")
`,
  efficacy: `  
  1. efficacy, efficaciousness -- (capacity or power to produce a desired effect; "concern about the safety and efficacy of the vaccine")
`,
  Empathy: ` 
  1. empathy -- (understanding and entering into another's feelings)
`,
  empowerment: ` 
  1. authorization, authorisation, empowerment -- (the act of conferring legality or sanction or formal warrant)
`,
  energy: `   
  1. energy, free energy -- ((physics) a thermodynamic quantity equivalent to the capacity of a physical system to do work; the units of energy are joules or ergs; "energy can take a wide variety of forms")
  2. energy, vigor, vigour, zip -- (forceful exertion; "he plays tennis with great energy"; "he's full of zip")
  3. energy, push, get-up-and-go -- (enterprising or ambitious drive; "Europeans often laugh at American energy")

`,
  enthusiasm: `   
  1. enthusiasm -- (a feeling of excitement)
  2. exuberance, enthusiasm, ebullience -- (overflowing with eager enjoyment or approval)
  3. enthusiasm -- (a lively interest; "enthusiasm for his program is growing")
`,
  equality: ` 
  1. equality -- (the quality of being the same in quantity or measure or value or status)
  2. equality, equivalence, equation, par -- (a state of being essentially equal or equivalent; equally balanced; "on a par with the best")
`,
  equanimity: `
  1. composure, calm, calmness, equanimity -- (steadiness of mind under stress; "he accepted their problems with composure and she with equanimity")
`,
  excellence: `   
  1. excellence -- (the quality of excelling; possessing good qualities in high degree)
  2. excellence, excellency -- (an outstanding feature; something in which something or someone excels; "a center of manufacturing excellence"; "the use of herbs is one of the excellencies of French cuisine")
`,
  exercise: ` 
  1. exercise, exercising, physical exercise, physical exertion, workout -- (the activity of exerting your muscles in various ways to keep fit; "the doctor recommended regular exercise"; "he did some exercising"; "the physical exertion required by his work kept him fit")
  2. use, usage, utilization, utilisation, employment, exercise -- (the act of using; "he warned against the use of narcotic drugs"; "skilled in the utilization of computers")
  3. exercise, practice, drill, practice session, recitation -- (systematic training by multiple repetitions; "practice makes perfect")

`,
  exuberance: ` 
  1. exuberance -- (joyful enthusiasm)
  2. exuberance, enthusiasm, ebullience -- (overflowing with eager enjoyment or approval)
`,
  fairness: ` 
  1. fairness, equity -- (conformity with rules or standards; "the judge recognized the fairness of my claim")
  2. fairness, fair-mindedness, candor, candour -- (ability to make judgments free from discrimination or dishonesty)
  3. paleness, blondness, fairness -- (the property of having a naturally light complexion)

`,
  fellowship: `   
  1. family, fellowship -- (an association of people who share common beliefs or activities; "the message was addressed not just to employees but to every member of the company family"; "the church welcomed new members into its fellowship")
  2. company, companionship, fellowship, society -- (the state of being with someone; "he missed their company"; "he enjoyed the society of his friends")
  3. fellowship -- (money granted (by a university or foundation or other agency) for advanced study or research)
`,
  flexibility: `  
  1. flexibility, flexibleness -- (the property of being flexible; easily bent or shaped)
  2. flexibility, flexibleness -- (the quality of being adaptable or variable; "he enjoyed the flexibility of his working arrangement")
  3. tractability, tractableness, flexibility -- (the trait of being easily persuaded)
`,
  flow: ` 
  1. flow, flowing -- (the motion characteristic of fluids (liquids or gases))
  2. flow, flow rate, rate of flow -- (the amount of fluid that flows in a given time)
  3. flow, stream -- (the act of flowing or streaming; continuous progression)

`,
  focus: `
  1. focus, focusing, focussing, focal point, direction, centering -- (the concentration of attention or energy on something; "the focus of activity shifted to molecular biology"; "he had no direction in his life")
  2. focus -- (maximum clarity or distinctness of an image rendered by an optical system; "in focus"; "out of focus")
  3. focus -- (maximum clarity or distinctness of an idea; "the controversy brought clearly into focus an important difference of opinion")

`,
  food: ` 
  1. food, nutrient -- (any substance that can be metabolized by an animal to give energy and build tissue)
  2. food, solid food -- (any solid substance (as opposed to liquid) that is used as a source of nourishment; "food and drink")
  3. food, food for thought, intellectual nourishment -- (anything that provides mental stimulus for thinking)
`,
  Freedom: `  
  1. freedom -- (the condition of being free; the power to act or speak or think without externally imposed restraints)
  2. exemption, freedom -- (immunity from an obligation or duty)
`,
  friendship: `
  1. friendship, friendly relationship -- (the state of being friends (or friendly))
`,
  fun: `  
  1. fun, merriment, playfulness -- (activities that are enjoyable or amusing; "I do it for the fun of it"; "he is fun to have around")
  2. fun, play, sport -- (verbal wit or mockery (often at another's expense but not to be taken seriously); "he became a figure of fun"; "he said it in sport")
  3. fun -- (violent and excited activity; "she asked for money and then the fun began"; "they began to fight like fun")

`,
  generosity: `   
  1. generosity, generousness -- (the trait of being willing to give your money or time)
  2. generosity, unselfishness -- (acting generously)
`,
  gratitude: ` 
  1. gratitude -- (a feeling of thankfulness and appreciation; "he was overwhelmed with gratitude for their help")
`,
  growth: `   
  1. growth, growing, maturation, development, ontogeny, ontogenesis -- ((biology) the process of an individual organism growing organically; a purely biological unfolding of events involved in an organism changing gradually from a simple to a more complex level; "he proposed an indicator of osseous development in children")
  2. growth -- (a progression from simpler to more complex forms; "the growth of culture")
  3. increase, increment, growth -- (a process of becoming larger or longer or more numerous or more important; "the increase in unemployment"; "the growth of population")

`,
  happiness: `
  1. happiness, felicity -- (state of well-being characterized by emotions ranging from contentment to intense joy)
  2. happiness -- (emotions experienced when in a state of well-being)
`,
  harmony: `  
  1. harmony, harmoniousness -- (compatibility in opinion and action)
  2. harmony, musical harmony -- (the structure of music with respect to the composition and progression of chords)
  3. harmony, concord, concordance -- (a harmonious state of things in general and of their properties (as of colors and sounds); congruity of parts with one another and with the whole)

`,
  Health: `   
  1. health, wellness -- (a healthy state of wellbeing free from disease; "physicians should be held responsible for the health of their patients")
  2. health -- (the general condition of body and mind; "his delicate health"; "in poor health")
`,
  help: ` 
  1. aid, assist, assistance, help -- (the activity of contributing to the fulfillment of a need or furtherance of an effort or purpose; "he gave me an assist with the housework"; "could not walk without assistance"; "rescue party went to their aid"; "offered his help in unloading")
  2. assistant, helper, help, supporter -- (a person who contributes to the fulfillment of a need or furtherance of an effort or purpose; "my invaluable assistant"; "they hired additional help to finish the work")
  3. aid, assistance, help -- (a resource; "visual aids in teaching")
`,
  home: ` 
  1. home, place -- (where you live at a particular time; "deliver the package to my home"; "he doesn't have a home to go to"; "your place or mine?")
  2. dwelling, home, domicile, abode, habitation, dwelling house -- (housing that someone is living in; "he built a modest dwelling near the pond"; "they raise money to provide homes for the homeless")
  3. home -- (the country or state or city where you live; "Canadian tariffs enabled United States lumber companies to raise prices at home"; "his home is New Jersey")

`,
  Honesty: `  
  1. honesty, honestness -- (the quality of being honest)
  2. honesty, silver dollar, money plant, satin flower, satinpod, Lunaria annua -- (southeastern European plant cultivated for its fragrant purplish flowers and round flat papery silver-white seedpods that are used for indoor decoration)
`,
  honoring: `
  1. honoring, observance -- (conformity with law or custom or practice etc.)
`,
  hospitality: `   
  1. cordial reception, hospitality -- (kindness in welcoming guests or strangers)
`,
  humor: `
  1. wit, humor, humour, witticism, wittiness -- (a message whose ingenuity or verbal skill or incongruity has the power to evoke laughter)
  2. humor, humour, sense of humor, sense of humour -- (the trait of appreciating (and being able to express) the humorous; "she didn't appreciate my humor"; "you can't survive in the army without a sense of humor")
  3. temper, mood, humor, humour -- (a characteristic (habitual or relatively temporary) state of feeling; "whether he praised or cursed me depended on his temper at the time"; "he was in a bad humor")

`,
  importance: `   
  1. importance -- (the quality of being important and worthy of note; "the importance of a well-balanced diet")
  2. importance, grandness -- (a prominent status; "a person of importance")
`,
  inclusion: `  
  1. inclusion -- (the state of being included)
  2. inclusion, comprehension -- (the relation of comprising something; "he admired the inclusion of so many ideas in such a short work")
  3. inclusion body, cellular inclusion, inclusion -- (any small intracellular body found within another (characteristic of certain diseases); "an inclusion in the cytoplasm of the cell")

`,
  independence: ` 
  1. independence, independency -- (freedom from control or influence of another or others)
  2. independence -- (the successful ending of the American Revolution; "they maintained close relations with England even after independence")
  3. Independence -- (a city in western Missouri; the beginning of the Santa Fe Trail)
`,
  innovation: `   
  1. invention, innovation -- (a creation (a new device or process) resulting from study and experimentation)
  2. invention, innovation, excogitation, conception, design -- (the creation of something in the mind)
  3. initiation, founding, foundation, institution, origination, creation, innovation, introduction, instauration -- (the act of starting something for the first time; introducing something new; "she looked forward to her initiation as an adult"; "the foundation of a new scientific society")
`,
  inspiration: `  
  1. inspiration -- (arousal of the mind to special unusual activity or creativity)
  2. inspiration, brainchild -- (a product of your creative thinking and work; "he had little respect for the inspirations of other artists"; "after years of work his brainchild was a tangible reality")
  3. inspiration -- (a sudden intuition as part of solving a problem)

`,
  integrity: `
  1. integrity, unity, wholeness -- (an undivided or unbroken completeness or totality with nothing wanting; "the integrity of the nervous system is required for normal development"; "he took measures to insure the territorial unity of Croatia")
  2. integrity -- (moral soundness; "he expects to find in us the common honesty and integrity of men of business"; "they admired his scrupulous professional integrity")
`,
  interdependence: `   
  1. mutuality, interdependence, interdependency -- (a reciprocal relation between interdependent entities (objects or individuals or groups))
`,
  intimacy: ` 
  1. familiarity, intimacy, closeness -- (close or warm friendship; "the absence of fences created a mysterious intimacy in which no one knew privacy")
  2. affair, affaire, intimacy, liaison, involvement, amour -- (a usually secretive or illicit sexual relationship)
  3. closeness, intimacy -- (a feeling of being intimate and belonging together; "their closeness grew as the night wore on")
`,
  joy: `  
  1. joy, joyousness, joyfulness -- (the emotion of great happiness)
  2. joy, delight, pleasure -- (something or someone that provides a source of happiness; "a joy to behold"; "the pleasure of his company"; "the new car is a delight")
`,
  justice: `  
  1. justice, justness -- (the quality of being just or fair)
  2. justice -- (judgment involved in the determination of rights and the assignment of rewards and punishments)
  3. judge, justice, jurist -- (a public official authorized to decide questions brought before a court of justice)

`,
  kindness: `   
  1. kindness -- (the quality of being warmhearted and considerate and humane and sympathetic)
  2. forgivingness, kindness -- (tendency to be kind and forgiving)
  3. kindness, benignity -- (a kind act)
`,
  learning: ` 
  1. learning, acquisition -- (the cognitive process of acquiring skill or knowledge; "the child's acquisition of language")
  2. eruditeness, erudition, learnedness, learning, scholarship, encyclopedism, encyclopaedism -- (profound scholarly knowledge)
`,
  love: ` 
  1. love -- (a strong positive emotion of regard and affection; "his love for his work"; "children need a lot of love")
  2. love, passion -- (any object of warm affection or devotion; "the theater was her first love"; "he has a passion for cock fighting";)
  3. beloved, dear, dearest, honey, love -- (a beloved person; used as terms of endearment)

`,
  mastery: `  
  1. command, control, mastery -- (great skillfulness and knowledge of some subject or activity; "a good command of French")
  2. domination, mastery, supremacy -- (power to dominate or defeat; "mastery of the seas")
  3. mastery, subordination -- (the act of mastering or subordinating someone)
`,
  mattering: ``,
  Meaning: `  
  1. meaning, significance, signification, import -- (the message that is intended or expressed or signified; "what is the meaning of this sentence"; "the significance of a red traffic light"; "the signification of Chinese characters"; "the import of his announcement was ambiguous")
  2. meaning, substance -- (the idea that is intended; "What is the meaning of this proverb?")
`,
  mourning: `   
  1. mourning, bereavement -- (state of sorrow over the death or departure of a loved one)
  2. lamentation, mourning -- (the passionate and demonstrative activity of expressing grief)
`,
  movement: `
  1. motion, movement, move, motility -- (a change of position that does not entail a change of location; "the reflex motion of his eyebrows revealed his surprise"; "movement is a sign of life"; "an impatient move of his hand"; "gastrointestinal motility")
  2. motion, movement, move -- (the act of changing location from one place to another; "police controlled the motion of the crowd"; "the movement of people from the farms to the cities"; "his move put him directly in my path")
  3. movement, motion -- (a natural event that involves a change in the position or location of something)

`,
  mutuality: `
  1. mutuality, mutualness -- (a reciprocality of sentiments; "the mutuality of their affection was obvious")
  2. mutuality, interdependence, interdependency -- (a reciprocal relation between interdependent entities (objects or individuals or groups))
`,
  nourishment: `  
  1. nutriment, nourishment, nutrition, sustenance, aliment, alimentation, victuals -- (a source of materials to nourish the body)
  2. nourishment -- (the act of nourishing; "her nourishment of the orphans saved many lives")
`,
  nurturing: ``,
  nutrition: `  
  1. nutrition -- ((physiology) the organic process of nourishing or being nourished; the processes by which an organism assimilates food and uses it for growth and maintenance)
  2. nutriment, nourishment, nutrition, sustenance, aliment, alimentation, victuals -- (a source of materials to nourish the body)
  3. nutrition -- (the scientific study of food and drink (especially in humans))
`,
  openness: `   
  1. openness -- (without obstructions to passage or view; "the openness of the prairies")
  2. openness, nakedness -- (characterized by an attitude of ready accessibility (especially about one's actions or purposes); without concealment; not secretive)
  3. receptiveness, receptivity, openness -- (willingness or readiness to receive (especially impressions or ideas); "he was testing the government's receptiveness to reform"; "this receptiveness is the key feature in oestral behavior, enabling natural mating to occur"; "their receptivity to the proposal")
`,
  order: `   
  1. order -- ((often plural) a command given by a superior (e.g., a military or law enforcement officer) that must be obeyed; "the British ships dropped anchor and waited for orders from London")
  2. order, order of magnitude -- (a degree in a continuum of size or quantity; "it was on the order of a mile"; "an explosion of a low order of magnitude")
  3. order -- (established customary state (especially of society); "order ruled in the streets"; "law and order")

`,
  partnership: `  
  1. partnership -- (the members of a business venture created by contract)
  2. partnership -- (a cooperative relationship between people or groups who agree to share responsibility for achieving some specific goal; "effective language learning is a partnership between school, teacher and student"; "the action teams worked in partnership with the government")
  3. partnership -- (a contract between two or more persons who agree to pool talent and money and share profits or losses)
`,
  passion: `  
  1. passion, passionateness -- (a strong feeling or emotion)
  2. heat, warmth, passion -- (the trait of being intensely emotional)
  3. rage, passion -- (something that is desired intensely; "his rage for fame destroyed him")

`,
  peace: `
  1. peace -- (the state prevailing during the absence of war)
  2. peace -- (harmonious relations; freedom from disputes; "the roommates lived in peace together")
  3. peace, peacefulness, peace of mind, repose, serenity, heartsease, ataraxis -- (the absence of mental stress or anxiety)

`,
  Play: `
  1. play, drama, dramatic play -- (a dramatic work intended for performance by actors on a stage; "he wrote several plays but only one was produced on Broadway")
  2. play -- (a theatrical performance of a drama; "the play lasted two hours")
  3. play -- (a preset plan of action in team sports; "the coach drew up the plays for her team")

`,
  power: `
  1. power, powerfulness -- (possession of controlling influence; "the deterrent power of nuclear weapons"; "the power of his love saved her"; "his powerfulness was concealed by a gentle facade")
  2. power -- ((physics) the rate of doing work; measured in watts (= joules/second))
  3. ability, power -- (possession of the qualities (especially mental qualities) required to do something or get something done; "danger heightened his powers of discrimination")

`,
  predictability: `  
  1. predictability -- (the quality of being predictable)
`,
  presence: ` 
  1. presence -- (the state of being present; current existence; "he tested for the presence of radon")
  2. presence, front -- (the immediate proximity of someone or something; "she blushed in his presence"; "he sensed the presence of danger"; "he was well behaved in front of company")
  3. presence -- (an invisible spiritual being felt to be nearby)

`,
  prizing: ``,
  prosperity: `   
  1. prosperity -- (an economic state of growth with rising profits and full employment)
  2. prosperity, successfulness -- (the condition of prospering; having good fortune)
`,
  "protection from harm": ``,
  purpose: `  
  1. purpose, intent, intention, aim, design -- (an anticipated outcome that is intended or that guides your planned actions; "his intent was to provide a new translation"; "good intentions are not enough"; "it was created with the conscious aim of answering immediate needs"; "he made no secret of his designs")
  2. function, purpose, role, use -- (what something is used for; "the function of an auger is to bore holes"; "ballet is beautiful but what use is it?")
  3. determination, purpose -- (the quality of being determined to do or achieve something; firmness of purpose; "his determination showed in his every movement"; "he is a man of purpose")
`,
  realness: `
  1. reality, realness, realism -- (the state of being actual or real; "the reality of his situation slowly dawned on him")
`,
  receptivity: ` 
  1. receptiveness, receptivity, openness -- (willingness or readiness to receive (especially impressions or ideas); "he was testing the government's receptiveness to reform"; "this receptiveness is the key feature in oestral behavior, enabling natural mating to occur"; "their receptivity to the proposal")
`,
  reciprocity: `
  1. reciprocality, reciprocity -- (a relation of mutual dependence or action or influence)
  2. reciprocity -- (mutual exchange of commercial or other privileges)
`,
  recognition: `  
  1. recognition, acknowledgment, acknowledgement -- (the state or quality of being recognized or acknowledged; "the partners were delighted with the recognition of their work"; "she seems to avoid much in the way of recognition or acknowledgement of feminist work prior to her own")
  2. recognition, identification -- (the process of recognizing something or someone by remembering; "a politician whose recall of names was as remarkable as his recognition of faces"; "experimental psychologists measure the elapsed time from the onset of the stimulus to its recognition by the observer")
  3. recognition, credit -- (approval; "give her recognition for trying"; "he was given credit for his work"; "give her credit for trying")

`,
  recreation: `   
  1. diversion, recreation -- (an activity that diverts or amuses or stimulates; "scuba diving is provided as a diversion for tourists"; "for recreation he wrote poetry and solved crossword puzzles"; "drug abuse is often regarded as a form of recreation")
  2. refreshment, recreation -- (activity that refreshes and recreates; activity that renews your health and spirits by enjoyment and relaxation; "time for rest and refreshment by the pool"; "days of joyous recreation with his friends")
`,
  rejuvenation: `   
  1. rejuvenation, greening -- (the phenomenon of vitality and freshness being restored; "the annual rejuvenation of the landscape")
  2. rejuvenation -- (the act of restoring to a more youthful condition)
`,
  relaxation: ` 
  1. relaxation -- ((physiology) the gradual lengthening of inactive muscle or muscle fibers)
  2. relaxation, relaxation behavior -- ((physics) the exponential return of a system to equilibrium after a disturbance)
  3. easiness, relaxation -- (a feeling of refreshing tranquility and an absence of tension or worry; "the easiness we feel when sleeping")

`,
  reliability: `   
  1. dependability, dependableness, reliability, reliableness -- (the quality of being dependable or reliable)
`,
  respect: `  
  1. respect, regard -- ((usually preceded by 'in') a detail or point; "it differs in that respect")
  2. esteem, regard, respect -- (the condition of being honored (esteemed or respected or well regarded); "it is held in esteem"; "a man who has earned high regard")
  3. respect, esteem, regard -- (an attitude of admiration or esteem; "she lost all respect for him")

`,
  rest: ` 
  1. remainder, balance, residual, residue, residuum, rest -- (something left after other parts have been taken away; "there was no remainder"; "he threw away the rest"; "he took what he wanted and I got the balance")
  2. rest, ease, repose, relaxation -- (freedom from activity (work or strain or responsibility); "took his repose by the swimming pool")
  3. respite, rest, relief, rest period -- (a pause for relaxation; "people actually accomplish more when they take time for short rests")
`,
  richness: ` 
  1. profusion, profuseness, richness, cornucopia -- (the property of being extremely abundant; "the profusion of detail"; "the idiomatic richness of English")
  2. affluence, richness -- (abundant wealth; "they studied forerunners of richness or poverty"; "the richness all around unsettled him for he had expected to find poverty")
  3. fullness, mellowness, richness -- (the property of a sensation that is rich and pleasing; "the music had a fullness that echoed through the hall"; "the cheap wine had no body, no mellowness"; "he was well aware of the richness of his own appearance")
`,
  Safety: `   
  1. safety -- (the state of being certain that adverse effects will not be caused by some agent under defined conditions; "insure the safety of the children"; "the reciprocal of safety is risk")
  2. safety, refuge -- (a safe place; "He ran to safety")
  3. guard, safety, safety device -- (a device designed to prevent injury or accidents)
`,
  security: ` 
  1. security -- (the state of being free from danger or injury; "we support the armed services in the name of national security")
  2. security, protection -- (defense against financial failure; financial independence; "his pension gave him security in his old age"; "insurance provided protection against loss of wages due to illness")
  3. security -- (freedom from anxiety or fear; "the watch dog gave her a feeling of security")
`,
  seeing: ` 
  1. visual perception, beholding, seeing -- (perception by means of the eyes)
  2. eyesight, seeing, sightedness -- (normal use of the faculty of vision)
`,
  "Self Expression": ` 
  1. self-expression -- (the expression of one's individuality (usually through creative activities))
`,
  "self-actualization": ``,
  sensitivity: `  
  1. sensitivity, sensitiveness, sensibility -- ((physiology) responsiveness to external stimuli; the faculty of sensation; "sensitivity to pain")
  2. sensitivity, sensitiveness -- (the ability to respond to physical stimuli or to register small physical amounts or differences; "a galvanometer of extreme sensitivity"; "the sensitiveness of Mimosa leaves does not depend on a change of growth")
  3. sensitivity, sensitiveness -- (sensitivity to emotional feelings (of self and others))

  `,
  serenity: ` 
  1. repose, quiet, placidity, serenity, tranquillity, tranquility -- (a disposition free from stress or emotion)
  2. peace, peacefulness, peace of mind, repose, serenity, heartsease, ataraxis -- (the absence of mental stress or anxiety)
`,
  shelter: `  
  1. shelter -- (a structure that provides privacy and protection from danger)
  2. shelter -- (protective covering that provides protection from the weather)
  3. protection, shelter -- (the condition of being protected; "they were huddled together for protection"; "he enjoyed a sense of peace and protection in his new home")
`,
  simplicity: `   
  1. simplicity, simpleness -- (the quality of being simple or uncompounded; "the simplicity of a crystal")
  2. simplicity, simpleness, simple mindedness -- (a lack of penetration or subtlety; "they took advantage of her simplicity")
  3. simplicity, simpleness -- (absence of affectation or pretense)

  `,
  skill: `
  1. skill, accomplishment, acquirement, acquisition, attainment -- (an ability that has been acquired by training)
  2. skill, science -- (ability to produce solutions in some problem domain; "the skill of a well-trained boxer"; "the sweet science of pugilism")
`,
  sleep: `
  1. sleep, slumber -- (a natural and periodic state of rest during which consciousness of the world is suspended; "he didn't get enough sleep last night"; "calm as a child in dreamless slumber")
  2. sleep, sopor -- (a torpid state resembling deep sleep)
  3. sleep, nap -- (a period of time spent sleeping; "he felt better after a little sleep"; "there wasn't time for a nap")
`,
  spontaneity: `   
  1. spontaneity, spontaneousness -- (the quality of being spontaneous and coming from natural feelings without constraint; "the spontaneity of his laughter")
`,
  stability: `
  1. stability, stableness -- (the quality or attribute of being firm and steadfast)
  2. stability -- (a stable order (especially of society))
  3. constancy, stability -- (the quality of being enduring and free from change or variation; "early mariners relied on the constancy of the trade winds")
`,
  structure: `
  1. structure, construction -- (a thing constructed; a complex entity constructed of many parts; "the structure consisted of a series of arches"; "she wore her hair in an amazing construction of whirls and ribbons")
  2. structure -- (the manner of construction of something and the arrangement of its parts; "artists must study the structure of the human body"; "the structure of the benzene molecule")
  3. structure -- (the complex composition of knowledge as elements and their combinations; "his lectures have no structure")
`,
  success: `  
  1. success -- (an event that accomplishes its intended purpose; "let's call heads a success and tails a failure"; "the election was a remarkable success for the Whigs")
  2. success -- (an attainment that is successful; "his success in the marathon was unexpected"; "his new play was a great success")
  3. success -- (a state of prosperity or fame; "he is enjoying great success"; "he does not consider wealth synonymous with success")
`,
  support: `
  1. support -- (the activity of providing for or maintaining by supplying with money or necessities; "his support kept the family together"; "they gave him emotional support during difficult times")
  2. support -- (aiding the cause or policy or interests of; "the president no longer has the support of his own party"; "they developed a scheme of mutual support")
  3. support -- (something providing immaterial assistance to a person or cause or interest; "the policy found little public support"; "his faith was all the support he needed"; "the team enjoyed the support of their fans")
`,
  sustainability: `  
  1. sustainability -- (the property of being sustainable)
`,
  synergy: ` 
  1. synergy, synergism -- (the working together of two things (muscles or drugs for example) to produce an effect greater than the sum of their individual effects)
`,
  touch: `   
  1. touch, touching -- (the event of something coming in contact with the body; "he longed for the touch of her hand"; "the cooling touch of the night air")
  2. touch, sense of touch, skin senses, touch modality, cutaneous senses -- (the faculty by which external objects or forces are perceived through contact with the body (especially the hands); "only sight and touch enable us to locate objects in the space around us")
  3. touch, trace, ghost -- (a suggestion of some quality; "there was a touch of sarcasm in his tone"; "he detected a ghost of a smile on her face")

`,
  tranquility: `  
  1. repose, quiet, placidity, serenity -- (a disposition free from stress or emotion)
  2. tranquillity, tranquility, quiet -- (an untroubled state; free from disturbances)
  3. tranquillity, tranquility, quietness, quietude -- (a state of peace and quiet)
`,
  trust: `
  1. trust -- (something (as property) held by one party (the trustee) for the benefit of another (the beneficiary); "he is the beneficiary of a generous trust set up by his father")
  2. reliance, trust -- (certainty based on past experience; "he wrote the paper with considerable reliance on the work of other scientists"; "he put more trust in his own two legs than in the gun")
  3. trust, trustingness, trustfulness -- (the trait of believing in the honesty and reliability of others; "the experience destroyed his trust and personal dignity")
`,
  "unconditional positive regard": ``,
  understanding: `
  1. understanding, apprehension, discernment, savvy -- (the cognitive condition of someone who understands; "he has virtually no understanding of social cause and effect")
  2. agreement, understanding -- (the statement (oral or written) of an exchange of promises; "they had an agreement that they would not interfere in each other's business"; "there was an understanding between management and the workers")
  3. sympathy, understanding -- (an inclination to support or be loyal to or to agree with an opinion; "his sympathies were always with the underdog"; "I knew I could count on his understanding")
`,
  valuing: ``,
  vision: `   
  1. vision -- (a vivid mental image; "he had a vision of his own death")
  2. sight, vision, visual sense, visual modality -- (the ability to see; the visual faculty)
  3. vision, visual sensation -- (the perceptual experience of seeing; "the runners emerged from the trees into his clear vision"; "he had a visual sensation of intense light")
`,
  vitality: ` 
  1. vitality, verve -- (an energetic style)
  2. energy, vim, vitality -- (a healthy capacity for vigorous activity; "jogging works off my excess energy"; "he seemed full of vim and vigor")
  3. life force, vital force, vitality, elan vital -- ((biology) a hypothetical force (not physical or chemical) once thought by Henri Bergson to cause the evolution and development of organisms)
`,
  warmth: `   
  1. heat, warmth -- (the sensation caused by heat energy)
  2. warmheartedness, warmth -- (a warmhearted feeling)
  3. warmth, warmness -- (the quality of having a moderate degree of heat; "an agreeable warmth in the house")

`,
  water: `
  1. water, H2O -- (binary compound that occurs at room temperature as a clear colorless odorless tasteless liquid; freezes into ice below 0 degrees centigrade and boils above 100 degrees centigrade; widely used as a solvent)
  2. body of water, water -- (the part of the earth's surface covered with water (such as a river or lake or ocean); "they invaded our territorial waters"; "they were sitting by the water's edge")
  3. water -- (once thought to be one of four elements composing the universe (Empedocles))
`,
  welcoming: ``,
  "Well Being": `
  1. wellbeing, well-being, welfare, upbeat, eudaemonia, eudaimonia -- (a contented state of being happy and healthy and prosperous; "the town was finally on the upbeat after our recent troubles")
`,
  wellness: `
  1. health, wellness -- (a healthy state of wellbeing free from disease; "physicians should be held responsible for the health of their patients")
`,
  wisdom: `   
  1. wisdom -- (accumulated knowledge or erudition or enlightenment)
  2. wisdom, wiseness -- (the trait of utilizing knowledge and experience with common sense and insight)
  3. wisdom, sapience -- (ability to apply knowledge or experience or understanding or common sense and insight)
`,
  wonder: `   
  1. wonder, wonderment, admiration -- (the feeling aroused by something strange and surprising)
  2. wonder, marvel -- (something that causes feelings of wonder; "the wonders of modern science")
  3. curiosity, wonder -- (a state in which you want to learn more about something)
`,
};

export function getNeedDefinition(name: string) {
  return (NeedDefinitions as any)[name] || "Definition needed";
}
