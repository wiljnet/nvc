export const FeelingDefinitions = {
  absorbed: `
  1. captive, absorbed, engrossed, enwrapped, intent, wrapped
  -- giving or marked by complete attention to
  -- "that engrossed look or rapt delight"
  -- "then wrapped in dreams"
  -- "so intent on this fantastic...narrative that she hardly stirred"- Walter de la Mare
  -- "rapt with wonder"
  -- "wrapped in thought"
`,
  adoring: `
1. adoring, doting, fond 
  -- extravagantly or foolishly loving and indulgent
  -- "adoring grandparents"
  -- "deceiving her preoccupied and doting husband with a young captain"
  -- "hopelessly spoiled by a fond mother"
  
`,
  adventurous: `
1. adventurous, adventuresome 
  -- willing to undertake or seeking out new and daring enterprises
  -- "adventurous pioneers"
  -- "the risks and gains of an adventuresome economy"
  
`,
  affectionate: `
1. affectionate, fond, lovesome, tender, warm 
  -- having or displaying warmth or affection
  -- "affectionate children"
  -- "a fond embrace"
  -- "fond of his nephew"
  -- "a tender glance"
  -- "a warm embrace"
  
`,
  afraid: `
1. afraid 
  -- filled with fear or apprehension
  -- "afraid even to turn his head"
  -- "suddenly looked afraid"
  -- "afraid for his life"
  -- "afraid of snakes"
  -- "afraid to ask questions"
  
`,
  agitated: `
1. agitated 
  -- troubled emotionally and usually deeply
  -- "agitated parents"
  
`,
  alert: `
1. alert, watchful 
  -- engaged in or accustomed to close observation
  -- "alert enough to spot the opportunity when it came"
  -- "constantly alert and vigilant, like a sentinel on duty"
  
2. alert, brisk, lively, merry, rattling, snappy, spanking, zippy 
  -- quick and energetic
  -- "a brisk walk in the park"
  -- "a lively gait"
  -- "a merry chase"
  -- "traveling at a rattling rate"
  -- "a snappy pace"
  -- "a spanking breeze"
  
3. alert, alive, awake 
  -- mentally perceptive and responsive;"an alert mind"
  -- "alert to the problems"
  -- "alive to what is going on"
  -- "awake to the dangers of her situation"
  -- "was now awake to the reality of his predicament"
  

`,
  alive: `
1. alive, live 
  -- possessing life
  -- "the happiest person alive"
  -- "the nerve is alive"
  -- "doctors are working hard to keep him alive"
  -- "a live canary"
  
2. alive 
  -- full of life and spirit
  -- "she was wonderfully alive for her age"
  -- "a face alive with mischief"
  
3. animated, alive 
  -- having life or vigor or spirit
  -- "an animated and expressive face"
  -- "animated conversation"
  -- "became very animated when he heard the good news    

`,
  amorous: `
1. amative, amorous 
  -- inclined toward or displaying love
  -- "feeling amorous"
  
2. amatory, amorous, romantic 
  -- expressive of or exciting sexual love or romance
  -- "her amatory affairs"
  -- "amorous glances"
  -- "a romantic adventure"
  -- "a romantic moonlight ride"
  

`,
  angry: `
1. angry 
  -- feeling or showing anger
  -- "angry at the weather"
  -- "angry customers"
  -- "an angry silence"
  -- "sending angry letters to the papers"
  

`,
  anguished: `
1. anguished, tormented, tortured 
  -- experiencing intense pain especially mental pain
  -- "an anguished conscience"
  -- "a small tormented schoolboy"
  -- "a tortured witness to another's humiliation"
    
`,
  animosity: `
1. animosity, animus, bad blood 
  -- a feeling of ill will arousing active hostility
    
`,
  anxious: `
1. anxious, dying 
  -- eagerly desirous
  -- "anxious to see the new show at the museum"
  -- "dying to hear who won"
  
2. anxious, nervous, queasy, uneasy, unquiet 
  -- causing or fraught with or showing anxiety
  -- "spent an anxious night waiting for the test results"
  -- "cast anxious glances behind her"
  -- "those nervous moments before takeoff"
  -- "an unquiet mind"
  

`,
  apathetic: `
1. apathetic 
  -- showing little or no emotion or animation
  -- "a woman who became active rather than apathetic as she grew older"
  
2. apathetic, indifferent 
  -- marked by a lack of interest
  -- "an apathetic audience"
  -- "the universe is neither hostile nor friendly
  -- it is simply indifferent"
  

`,
  appreciative: `
1. appreciative 
  -- feeling or expressive of gratitude
  -- "was appreciative of his efforts"
  -- "an appreciative word"
  
2. appreciative 
  -- having or showing appreciation or a favorable critical judgment or opinion
  -- "appreciative of a beautiful landscape"
  -- "an appreciative laugh from the audience"
  

`,
  apprehensive: `
1. apprehensive, discerning 
  -- quick to understand
  
2. apprehensive, worried 
  -- mentally upset over possible misfortune or danger etc
  -- "apprehensive about her job"
  -- "not used to a city and worried about small things"
  -- "felt apprehensive about the consequences"
  
3. apprehensive 
  -- in fear or dread of possible evil or harm
  -- "apprehensive for one's life"
  -- "apprehensive of danger"
  

`,
  aroused: `
1. aroused 
  -- aroused to action
  -- "the aroused opposition"
  
2. stimulated, stirred, stirred up, aroused 
  -- emotionally aroused
  
3. aroused, wound up 
  -- brought to a state of great tension
  -- "all wound up for a fight"

  

`,
  astonished: `
1. amazed, astonied, astonished, astounded, stunned 
  -- filled with the emotional impact of overwhelming surprise or shock
  -- "an amazed audience gave the magician a standing ovation"
  -- "I stood enthralled, astonished by the vastness and majesty of the cathedral"
  -- "astounded viewers wept at the pictures from the Oklahoma City bombing"
  -- "stood in stunned silence"
  -- "stunned scientists found not one but at least three viruses"
    
`,
  bitter: `
1. acrimonious, bitter 
  -- marked by strong resentment or cynicism
  -- "an acrimonious dispute"
  -- "bitter about the divorce"
  
2. bitter 
  -- very difficult to accept or bear
  -- "the bitter truth"
  -- "a bitter sorrow"
  
5. bitter 
  -- proceeding from or exhibiting great hostility or animosity
  -- "a bitter struggle"
  -- "bitter enemies"
  


`,
  blah: `
`,
  blissful: `
1. blissful 
  -- completely happy and contented
  -- "blissful young lovers"
  -- "in blissful ignorance"
    
`,
  blue: `
1. gloomy, grim, blue, depressed, dispirited, down, downcast, downhearted, down in the mouth, low, low-spirited 
  -- filled with melancholy and despondency 
  -- "gloomy at the thought of what he had to face"
  -- "gloomy predictions"
  -- "a gloomy silence"
  -- "took a grim view of the economy"
  -- "the darkening mood"
  -- "lonely and blue in a strange city"
  -- "depressed by the loss of his job"
  -- "a dispirited and resigned expression on her face"
  -- "downcast after his defeat"
  -- "feeling discouraged and downhearted"

`,
  boggled: `
1. boggle 
  -- startle with amazement or fear
  
2. boggle 
  -- hesitate when confronted with a problem, or when in doubt or fear
  
3. flabbergast, boggle, bowl over 
  -- overcome with amazement
  -- "This boggles the mind!"
  

`,
  bored: `
1. bored, world-weary 
  -- tired of the world
  -- "bored with life"
  -- "strolled through the museum with a bored air"
  
2. blase, bored 
  -- uninterested because of frequent exposure or indulgence
  -- "his blase indifference"
  -- "a petulant blase air"
  -- "the bored gaze of the successful film star"
  

`,
  buoyant: `
1. buoyant, floaty 
  -- tending to float on a liquid or rise in air or gas
  -- "buoyant balloons"
  -- "buoyant balsa wood boats"
  -- "a floaty scarf"
  
2. buoyant, chirpy, perky 
  -- characterized by liveliness and lightheartedness
  -- "buoyant spirits"
  -- "his quick wit and chirpy humor"
  -- "looking bright and well and chirpy"
  -- "a perky little widow in her 70s"
  

`,
  calm: `
1. calm, unagitated, serene, tranquil 
  -- not agitated
  -- without losing self-possession
  -- "spoke in a calm voice"
  -- "remained calm throughout the uproar"
  -- "he remained serene in the midst of turbulence"
  -- "a serene expression on her face"
  -- "she became more tranquil"
  -- "tranquil life in the country"    

`,
  cantankerous: `
1. bloody-minded, cantankerous 
  -- stubbornly obstructive and unwilling to cooperate
  
2. cantankerous, crotchety, ornery 
  -- having a difficult and contrary disposition
  -- "a cantankerous and venomous-tongued old lady"- Dorothy Sayers
  

`,
  carefree: `
1. carefree, unworried 
  -- free of trouble and worry and care
  -- "the carefree joys of childhood"
  -- "carefree millionaires, untroubled financially"
  
2. carefree, devil-may-care, freewheeling, happy-go-lucky, harum-scarum, slaphappy 
  -- cheerfully irresponsible
  -- "carefree with his money"
  -- "freewheeling urban youths"
  -- "had a harum-scarum youth"
  

`,
  chagrined: `
1. abashed, chagrined, embarrassed 
  -- feeling or caused to feel uneasy and self-conscious
  -- "felt abashed at the extravagant praise"
  -- "chagrined at the poor sales of his book"
  -- "was embarrassed by her child's tantrums"
    
`,
  comatose: `
1. comatose 
  -- relating to or associated with a coma
  -- "comatose breathing"
  -- "comatose state"
  
2. comatose 
  -- in a state of deep and usually prolonged unconsciousness
  -- unable to respond to external stimuli
  -- "a comatose patient"
  

`,
  compassionate: `
1. compassionate 
  -- showing or having compassion
  -- "heard the soft and compassionate voices of women"
  
`,
  composed: `
1. composed 
  -- serenely self-possessed and free from agitation especially in times of stress
  -- "the performer seemed completely composed as she stepped onto the stage"
  -- "I felt calm and more composed than I had in a long time"
    
`,
  concerned: `
1. concerned 
  -- feeling or showing worry or solicitude
  -- "concerned relatives of those at risk"
  -- "was concerned about the future"
  -- "we feel concerned about accomplishing the task at hand"
  -- "greatly concerned not to disappoint a small child"
  
`,
  confident: `
1. confident 
  -- having or marked by confidence or assurance
  -- "a confident speaker"
  -- "a confident reply"
  -- "his manner is more confident these days"
  -- "confident of fulfillment"
  
2. convinced, positive, confident 
  -- persuaded of
  -- very sure
  -- "were convinced that it would be to their advantage to join"
  -- "I am positive he is lying"
  -- "was confident he would win"
  
3. confident, surefooted, sure-footed 
  -- not liable to error in judgment or action
  -- "most surefooted of the statesmen who dealt with the depression"- Walter Lippman
  -- "demonstrates a surefooted storytelling talent"- Michiko Kakutani
  

`,
  content: `
1. contented, content 
  -- satisfied or showing satisfaction with things as they are
  -- "a contented smile"
  
`,
  curious: `
1. curious 
  -- eager to investigate and learn or learn more
  -- "a curious child is a teacher's delight"
  -- "a trap door that made me curious"
  -- "curious investigators"
  -- "traffic was slowed by curious rubberneckers"
  -- "curious about the neighbor's doings"
  
`,
  dejected: `
1. dejected 
  -- affected or marked by low spirits
  -- "is dejected but trying to look cheerful"
  
`,
  delighted: `
1. delighted 
  -- greatly pleased
  
2. beguiled, captivated, charmed, delighted, enthralled, entranced 
  -- filled with wonder and delight
  

`,
  depressed: `
1. gloomy, grim, blue, depressed, dispirited, down, downcast, downhearted, down in the mouth, low, low-spirited 
  -- filled with melancholy and despondency 
  -- "gloomy at the thought of what he had to face"
  -- "gloomy predictions"
  -- "a gloomy silence"
  -- "took a grim view of the economy"
  -- "the darkening mood"
  -- "lonely and blue in a strange city"
  -- "depressed by the loss of his job"
  -- "a dispirited and resigned expression on her face"
  -- "downcast after his defeat"
  -- "feeling discouraged and downhearted"
   
`,
  despairing: `
1. despairing, desperate 
  -- arising from or marked by despair or loss of hope
  -- "a despairing view of the world situation"
  -- "the last despairing plea of the condemned criminal"
  -- "a desperate cry for help"
  -- "helpless and desperate--as if at the end of his tether"
  -- "her desperate screams"
    
`,
  desperate: `
1. despairing, desperate 
  -- arising from or marked by despair or loss of hope
  -- "a despairing view of the world situation"
  -- "the last despairing plea of the condemned criminal"
  -- "a desperate cry for help"
  -- "helpless and desperate--as if at the end of his tether"
  -- "her desperate screams"
  
2. desperate, do-or-die 
  -- desperately determined
  -- "do-or-die revolutionaries"
  -- "a do-or-die conflict"
  
3. desperate 
  -- showing extreme urgency or intensity especially because of great need or desire
  -- "felt a desperate urge to confess"
  -- "a desperate need for recognition"
  
6. desperate, dire 
  -- fraught with extreme danger
  -- nearly hopeless
  -- "a desperate illness"
  -- "on all fronts the Allies were in a desperate situation due to lack of materiel"- G.C.Marshall
  -- "a dire emergency"
  

`,
  despondent: `
1. despondent, heartsick 
  -- without or almost without hope
  -- "despondent about his failure"
  -- "too heartsick to fight back"
    
`,
  detached: `
1. degage, detached, uninvolved 
  -- showing lack of emotional involvement
  -- "adopted a degage pose on the arm of the easy chair"- J.S.Perelman
  -- "she may be detached or even unfeeling but at least she's not hypocritically effusive"
  -- "an uninvolved bystander"
  
2. detached, isolated, separated, set-apart 
  -- being or feeling set or kept apart from others
  -- "she felt detached from the group"
  -- "could not remain the isolated figure he had been"- Sherwood Anderson
  -- "thought of herself as alone and separated from the others"
  -- "had a set-apart feeling"
 
 

`,
  discouraged: `
1. demoralized, demoralised, discouraged, disheartened 
  -- made less hopeful or enthusiastic
  -- "desperate demoralized people looking for work"
  -- "felt discouraged by the magnitude of the problem"
  -- "the disheartened instructor tried vainly to arouse their interest"
  
2. discouraged 
  -- lacking in resolution
  -- "the accident left others discouraged about going there"
  

`,
  disgruntled: `
1. disgruntled, dissatisfied 
  -- in a state of sulky dissatisfaction
   
`,
  disgusted: `
1. disgusted, fed up, sick, sick of, tired of 
  -- having a strong distaste from surfeit
  -- "grew more and more disgusted"
  -- "fed up with their complaints"
  -- "sick of it all"
  -- "sick to death of flattery"
  -- "gossip that makes one sick"
  -- "tired of the noise and smoke"
    
`,
  disheartened: `
1. demoralized, demoralised, discouraged, disheartened 
  -- made less hopeful or enthusiastic
  -- "desperate demoralized people looking for work"
  -- "felt discouraged by the magnitude of the problem"
  -- "the disheartened instructor tried vainly to arouse their interest"
    
`,
  disinterested: `
1. disinterested 
  -- unaffected by self-interest
  
`,
  dismayed: `
1. aghast, appalled, dismayed, shocked 
  -- struck with fear, dread, or consternation
    
`,
  distant: `
1. aloof, distant, upstage 
  -- remote in manner
  -- "stood apart with aloof dignity"
  -- "a distant smile"
  -- "he was upstage with strangers"
   
`,
  distressed: `
1. disquieted, distressed, disturbed, upset, worried 
  -- afflicted with or marked by anxious uneasiness or trouble or grief
  -- "too upset to say anything"
  -- "spent many disquieted moments"
  -- "distressed about her son's leaving home"
  -- "lapsed into disturbed sleep"
  -- "worried parents"
  -- "a worried frown"
  -- "one last worried check of the sleeping children"
    
`,
  dread: `
1. awful, dire, direful, dread, dreaded, dreadful, fearful, fearsome, frightening, horrendous, horrific, terrible 
  -- causing fear or dread or terror
  -- "the awful war"
  -- "an awful risk"
  -- "dire news"
  -- "a career or vengeance so direful that London was shocked"
  -- "the dread presence of the headmaster"
  -- "polio is no longer the dreaded disease it once was"
  -- "a dreadful storm"
  -- "a fearful howling"
  -- "horrendous explosions shook the city"
  -- "a terrible curse"
   
`,
  dull: `
1. dull 
  -- lacking in liveliness or animation
  -- "he was so dull at parties"
  -- "a dull political campaign"
  -- "a large dull impassive man"
  -- "dull days with nothing to do"
  -- "how dull and dreary the world is"
  -- "fell back into one of her dull moods"
  
2. dull, slow, sluggish 
  -- "business is dull (or slow)"
  -- "a sluggish market"
  
3. dull 
  -- blunted in responsiveness or sensibility
  -- "a dull gaze"
  -- "so exhausted she was dull to what went on about her"- Willa Cather


`,
  eager: `
1. eager 
  -- having or showing keen interest or intense desire or impatient expectancy
  -- "eager to learn"
  -- "eager to travel abroad"
  -- "eager for success"
  -- "eager helpers"
  -- "an eager look"
    
`,
  ecstatic: `
1. ecstatic, enraptured, rapturous, rapt, rhapsodic 
  -- feeling great rapture or delight
    
`,
  edgy: `
1. edgy, high-strung, highly strung, jittery, jumpy, nervy, overstrung, restive, uptight 
  -- being in a tense state
    
`,
  effervescent: `
1. bubbling, effervescent, frothy, scintillating, sparkly 
  -- marked by high spirits or excitement
  -- "his fertile effervescent mind"
  -- "scintillating personality"
  -- "a row of sparkly cheerleaders"
    
`,
  electrified: `
1. electrify 
  -- excite suddenly and intensely
  -- "The news electrified us"
    
`,
  embarrassed: `
1. abashed, chagrined, embarrassed 
  -- feeling or caused to feel uneasy and self-conscious
  -- "felt abashed at the extravagant praise"
  -- "chagrined at the poor sales of his book"
  -- "was embarrassed by her child's tantrums"
  
2. embarrassed, humiliated, mortified 
  -- made to feel uncomfortable because of shame or wounded pride
  -- "too embarrassed to say hello to his drunken father on the street"
  -- "humiliated that his wife had to go out to work"
  -- "felt mortified by the comparison with her sister"
  

`,
  encouraged: `
1. bucked up, encouraged 
  -- inspired with confidence
  -- "felt bucked up by his success"
    
`,
  energetic: `
1. energetic 
  -- possessing or exerting or displaying energy
  -- "an energetic fund raiser for the college"
  -- "an energetic group of hikers"
  -- "it caused an energetic chemical reaction"
  
`,
  engrossed: `
1. captive, absorbed, engrossed, enwrapped, intent, wrapped 
  -- giving or marked by complete attention to
  -- "that engrossed look or rapt delight"
  -- "then wrapped in dreams"
  -- "so intent on this fantastic...narrative that she hardly stirred"- Walter de la Mare
  -- "rapt with wonder"
  -- "wrapped in thought"
  
`,
  enraged: `
1. angered, enraged, furious, infuriated, maddened 
  -- marked by extreme anger
  -- "the enraged bull attached"
  -- "furious about the accident"
  -- "a furious scowl"
  -- "infuriated onlookers charged the police who were beating the boy"
  -- "could not control the maddened crowd"
  
`,
  enriched: `
1. enrich 
  -- make better or improve in quality
  -- "The experience enriched her understanding"
  -- "enriched foods"
  
`,
  enthusiastic: `
1. enthusiastic 
  -- having or showing great excitement and interest
  -- "enthusiastic crowds filled the streets"
  -- "an enthusiastic response"
  -- "was enthusiastic about taking ballet lessons"
  
`,
  exasperated: `
1. exasperated, cheesed off, browned off 
  -- greatly annoyed
  -- out of patience
  -- "had an exasperated look on his face"
  -- "felt exasperated beyond endurance"
   
`,
  excited: `
1. aroused, emotional, excited, worked up 
  -- excessively affected by emotion
  -- "he would become emotional over nothing at all"
  -- "she was worked up about all the noise"
  
2. delirious, excited, frantic, mad, unrestrained 
  -- marked by uncontrolled excitement or emotion
  -- "a crowd of delirious baseball fans"
  -- "something frantic in their gaiety"
  -- "a mad whirl of pleasure"
  

`,
  exhausted: `
1. exhausted, dog-tired, fagged, fatigued, played out, spent, washed-out, worn-out, worn out 
  -- drained of energy or effectiveness
  -- extremely tired
  -- completely exhausted
  -- "the day's shopping left her exhausted"
  -- "he went to bed dog-tired"
  -- "was fagged and sweaty"
  -- "the trembling of his played out limbs"
  -- "felt completely washed-out"
  -- "only worn-out horses and cattle"
  -- "you look worn out"
  
`,
  exhilarated: `
1. gladdened, exhilarated 
  -- made joyful
  -- "the sun and the wind on his back made him feel exhilarated--happy to be alive"
   
`,
  expansive: `
1. expansive, talkative 
  -- friendly and open and willing to talk
  -- "wine made the guest expansive"
  
`,
  exuberant: `
1. ebullient, exuberant, high-spirited 
  -- joyously unrestrained
  
2. excessive, extravagant, exuberant, overweening 
  -- unrestrained, especially with regard to feelings
  -- "extravagant praise"
  -- "exuberant compliments"
  -- "overweening ambition"
  -- "overweening greed"
  
3. exuberant, lush, luxuriant, profuse, riotous 
  -- produced or growing in extreme abundance
  -- "their riotous blooming"
  

`,
  fascinated: `
1. fascinated, hypnotized, hypnotised, mesmerized, mesmerised, spellbound, spell-bound, transfixed 
  -- having your attention fixated as though by a spell
   
`,
  fatigued: `
1. exhausted, dog-tired, fagged, fatigued, played out, spent, washed-out, worn-out, worn out 
  -- drained of energy or effectiveness
  -- extremely tired
  -- completely exhausted
  -- "the day's shopping left her exhausted"
  -- "he went to bed dog-tired"
  -- "was fagged and sweaty"
  -- "the trembling of his played out limbs"
  -- "felt completely washed-out"
  -- "only worn-out horses and cattle"
  -- "you look worn out"
    
`,
  fearful: `
1. fearful 
  -- experiencing or showing fear
  -- "a fearful glance"
  -- "fearful of criticism"
   
`,
  fidgety: `
1. antsy, fidgety, fretful, itchy 
  -- nervous and unable to relax
  -- "a constant fretful stamping of hooves"
  -- "a restless child"
   
`,
  friendly: `
1. friendly, favorable, well-disposed 
  -- inclined to help or support
  -- not antagonistic or hostile
  -- "a government friendly to our interests"
  -- "an amicable agreement"
  
`,
  frightened: `
1. frightened, scared 
  -- made afraid
  -- "the frightened child cowered in the corner"
  -- "too shocked and scared to move"
  
`,
  frustrated: `
1. defeated, disappointed, discomfited, foiled, frustrated, thwarted 
  -- disappointingly unsuccessful
  -- "disappointed expectations and thwarted ambitions"
  -- "their foiled attempt to capture Calais"
  -- "many frustrated poets end as pipe-smoking teachers"
  -- "his best efforts were thwarted"
    
`,
  fulfilled: `
1. fulfilled 
  -- completed to perfection
  
`,
  furious: `
1. angered, enraged, furious, infuriated, maddened 
  -- marked by extreme anger
  -- "the enraged bull attached"
  -- "furious about the accident"
  -- "a furious scowl"
  -- "infuriated onlookers charged the police who were beating the boy"
  -- "could not control the maddened crowd"
  
`,
  giddy: `
1. dizzy, giddy, woozy, vertiginous 
  -- having or causing a whirling sensation
  -- liable to falling
  -- "had a dizzy spell"
  -- "a dizzy pinnacle"
  -- "had a headache and felt giddy"
  -- "a giddy precipice"
  -- "feeling woozy from the blow on his head"
  -- "a vertiginous climb up the face of the cliff"
  
2. airheaded, dizzy, empty-headed, featherbrained, giddy, light-headed, lightheaded, silly 
  -- lacking seriousness
  -- given to frivolity
  -- "a dizzy blonde"
  -- "light-headed teenagers"
  -- "silly giggles"
  

`,
  gloomy: `
1. glooming, gloomy, gloomful, sulky 
  -- depressingly dark
  -- "the gloomy forest"
  -- "the glooming interior of an old inn"
  -- "'gloomful' is archaic"
  
2. gloomy, grim, blue, depressed, dispirited, down, downcast, downhearted, down in the mouth, low, low-spirited 
  -- filled with melancholy and despondency 
  -- "gloomy at the thought of what he had to face"
  -- "gloomy predictions"
  -- "a gloomy silence"
  -- "took a grim view of the economy"
  -- "the darkening mood"
  -- "lonely and blue in a strange city"
  -- "depressed by the loss of his job"
  -- "a dispirited and resigned expression on her face"
  -- "downcast after his defeat"
  -- "feeling discouraged and downhearted"
  
3. blue, dark, dingy, disconsolate, dismal, gloomy, grim, sorry, drab, drear, dreary 
  -- causing dejection
  -- "a blue day"
  -- "the dark days of the war"
  -- "a week of rainy depressing weather"
  -- "a disconsolate winter landscape"
  -- "the first dismal dispiriting days of November"
  -- "a dark gloomy day"
  -- "grim rainy weather"
  

`,
  glorious: `
1. glorious 
  -- having or deserving or conferring glory
  -- "a long and glorious career"
  -- "our glorious literature"
  
2. brilliant, glorious, magnificent, splendid 
  -- characterized by grandeur
  -- "the brilliant court life at Versailles"
  -- "a glorious work of art"
  -- "magnificent cathedrals"
  -- "the splendid coronation ceremony"
  
3. glorious, resplendent, splendid, splendiferous 
  -- having great beauty and splendor
  -- "a glorious spring morning"
  -- "a glorious sunset"
  -- "splendid costumes"
  -- "a kind of splendiferous native simplicity"
  

`,
  goofy: `
1. cockamamie, cockamamy, goofy, sappy, silly, wacky, whacky, zany 
  -- ludicrous, foolish
  -- "gave me a cockamamie reason for not going"
  -- "wore a goofy hat"
  -- "a silly idea"
  -- "some wacky plan for selling more books"
  

`,
  grateful: `
1. grateful, thankful 
  -- feeling or showing gratitude
  -- "a grateful heart"
  -- "grateful for the tree's shade"
  -- "a thankful smile"
  
2. grateful 
  -- affording comfort or pleasure
  -- "the grateful warmth of the fire"
  

`,
  grouchy: `
1. crabbed, crabby, cross, fussy, grouchy, grumpy, bad-tempered, ill-tempered 
  -- annoyed and irritable
  

`,
  happy: `
1. happy 
  -- enjoying or showing or marked by joy or pleasure
  -- "a happy smile"
  -- "spent many happy days on the beach"
  -- "a happy marriage"
  
2. felicitous, happy 
  -- marked by good fortune
  -- "a felicitous life"
  -- "a happy outcome"
  
3. glad, happy 
  -- eagerly disposed to act or to be of service
  -- "glad to help"
  

`,
  heavy: `
1. heavy 
  -- marked by great psychological weight
  -- weighted down especially with sadness or troubles or weariness
  -- "a heavy heart"
  -- "a heavy schedule"
  -- "heavy news"
  -- "a heavy silence"
  -- "heavy eyelids"
  
2. heavy, lowering, sullen, threatening 
  -- darkened by clouds
  -- "a heavy sky"
  
3. dense, heavy, impenetrable 
  -- permitting little if any light to pass through because of denseness of matter
  -- "dense smoke"
  -- "heavy fog"
  -- "impenetrable gloom"

 

`,
  helpful: `
1. helpful 
  -- providing assistance or serving a useful function
  

`,
  helpless: `
1. helpless, incapacitated 
  -- lacking in or deprived of strength or power
  -- "lying ill and helpless"
  -- "helpless with laughter"
  
2. helpless, lost 
  -- unable to function
  -- without help
  
3. helpless 
  -- unable to manage independently
  -- "as helpless as a baby"
  

`,
  hesitant: `
1. hesitant, hesitating 
  -- lacking decisiveness of character
  -- unable to act or decide quickly or firmly
  

`,
  hopeful: `
1. hopeful 
  -- having or manifesting hope
  -- "a line of people hopeful of obtaining tickets"
  -- "found a hopeful way of attacking the problem"
  
2. bright, hopeful, promising 
  -- full or promise
  -- "had a bright future in publishing"
  -- "the scandal threatened an abrupt end to a promising political career"
  -- "a hopeful new singer on Broadway"
  

`,
  horrified: `
1. horrified, horror-stricken, horror-struck 
  -- stricken with horror
  

`,
  hostile: `
1. hostile 
  -- characterized by enmity or ill will
  -- "a hostile nation"
  -- "a hostile remark"
  -- "hostile actions"
  
2. hostile, uncongenial, unfriendly 
  -- very unfavorable to life or growth
  -- "a hostile climate"
  -- "an uncongenial atmosphere"
  -- "an uncongenial soil"
  -- "the unfriendly environment at high altitudes"
  

`,
  hurt: `
1. hurt, wounded 
  -- suffering from physical injury especially that suffered in battle
  -- "nursing his wounded arm"
  -- "ambulances...for the hurt men and women"

`,
  impatient: `
1. impatient 
  -- restless or short-tempered under delay or opposition
  -- "impatient with the slower students"
  -- "impatient of criticism"
  
2. impatient, raring 
  -- "impatient to begin"
  -- "raring to go"
  

`,
  impish: `
1. arch, impish, implike, mischievous, pixilated, prankish, puckish, wicked 
  -- naughtily or annoyingly playful
  -- "teasing and worrying with impish laughter"
  -- "a wicked prank"
  

`,
  indifferent: `
1. apathetic, indifferent 
  -- marked by a lack of interest
  -- "an apathetic audience"
  -- "the universe is neither hostile nor friendly
  -- it is simply indifferent"
  
2. indifferent 
  -- showing no care or concern in attitude or action
  -- "indifferent to the sufferings of others"
  -- "indifferent to her plea"
  
3. immaterial, indifferent 
  -- not mattering one way or the other
  -- "whether you choose to do it or not is a matter that is quite immaterial (or indifferent)"
  -- "what others think is altogether indifferent to him"
      

`,
  inert: `
1. inert 
  -- unable to move or resist motion
  
2. inert, indifferent, neutral 
  -- having only a limited ability to react chemically
  -- chemically inactive
  -- "inert matter"
  -- "an indifferent chemical in a reaction"
  
3. inert, sluggish, soggy, torpid 
  -- slow and apathetic
  -- "she was fat and inert"
  -- "a sluggish worker"
  -- "a mind grown torpid in old age"
  

`,
  inquisitive: `
1. inquisitive, speculative, questioning, wondering 
  -- showing curiosity
  -- "if someone saw a man climbing a light post they might get inquisitive"
  -- "raised a speculative eyebrow"
  
2. inquisitive 
  -- inquiring or appearing to inquire
  -- "an inquiring look"
  -- "the police are proverbially inquisitive"
  

`,
  insecure: `
1. insecure 
  -- not firm or firmly fixed
  -- likely to fail or give way
  -- "the hinge is insecure"
  
2. insecure, unsafe 
  -- lacking in security or safety
  -- "his fortune was increasingly insecure"
  -- "an insecure future"
  
3. insecure 
  -- lacking self-confidence or assurance
  -- "an insecure person lacking mental stability"
  

`,
  inspired: `
1. divine, elysian, inspired 
  -- being of such surpassing excellence as to suggest inspiration by the gods
  -- "her pies were simply divine"
  -- "the divine Shakespeare"
  -- "an elysian meal"
  -- "an inspired performance"
  

`,
  intense: `
1. intense 
  -- possessing or displaying a distinctive feature to a heightened degree
  -- "intense heat"
  -- "intense anxiety"
  -- "intense desire"
  -- "intense emotion"
  -- "the skunk's intense acrid odor"
  -- "intense pain"
  -- "enemy fire was intense"
  
2. acute, intense 
  -- extremely sharp or intense
  -- "acute pain"
  -- "felt acute annoyance"
  -- "intense itching and burning"
  
3. intense, vivid 
  -- "vivid green"
  -- "intense blue"
  

`,
  intrigued: `
1. intrigued, fascinated 
  -- interested or curious

`,
  invigorated: `
1. fresh, invigorated, refreshed, reinvigorated 
  -- with restored energy  

`,
  involved: `
1. involved 
  -- connected by participation or association or use
  -- "we accomplished nothing, simply because of the large number of people involved"
  -- "the problems involved"
  -- "the involved muscles"
  -- "I don't want to get involved"
  -- "everyone involved in the bribery case has been identified"
  
2. involved, mired 
  -- entangled or hindered as if e.g. in mire
  -- "the difficulties in which the question is involved"
  -- "brilliant leadership mired in details and confusion"
  
3. involved 
  -- emotionally involved
      

`,
  irate: `
1. irate, ireful 
  -- feeling or showing extreme anger
  -- "irate protesters"
  -- "ireful words"
  

`,
  irked: `
1. gall, irk 
  -- irritate or vex
  -- "It galls me that we lost the suit"
  

`,
  irritable: `
1. cranky, fractious, irritable, nettlesome, peevish, peckish, pettish, petulant, scratchy, testy, tetchy, techy 
  -- easily irritated or annoyed
  -- "an incorrigibly fractious young man"
  -- "not the least nettlesome of his countrymen"
  
2. irritable 
  -- abnormally sensitive to a stimulus
  
3. excitable, irritable 
  -- capable of responding to stimuli
  

`,
  irritated: `
1. annoyed, irritated, miffed, nettled, peeved, pissed, pissed off, riled, roiled, steamed, stung 
  -- aroused to impatience or anger
  -- "made an irritated gesture"
  -- "feeling nettled from the constant teasing"
  -- "peeved about being left out"
  -- "felt really pissed at her snootiness"
  -- "riled no end by his lies"
  -- "roiled by the delay"
  

`,
  jealous: `
1. covetous, envious, jealous 
  -- showing extreme cupidity
  -- painfully desirous of another's advantages
  -- "he was never covetous before he met her"
  -- "jealous of his success and covetous of his possessions"
  -- "envious of their art collection"
  
2. jealous, green-eyed, overjealous 
  -- suspicious or unduly suspicious or fearful of being displaced by a rival
  -- "a jealous lover"
  

`,
  jittery: `
1. jittery 
  -- characterized by jerky movements
  -- "a jittery ride"
  
2. edgy, high-strung, highly strung, jittery, jumpy, nervy, overstrung, restive, uptight 
  -- being in a tense state
  

`,
  joyful: `
1. joyful 
  -- full of or producing joy
  -- "make a joyful noise"
  -- "a joyful occasion"
  
2. elated, gleeful, joyful, jubilant 
  -- full of high-spirited delight
  -- "a joyful heart"
  

`,
  jubilant: `
1. exultant, exulting, jubilant, prideful, rejoicing, triumphal, triumphant 
  -- joyful and proud especially because of triumph or success
  -- "rejoicing crowds filled the streets on VJ Day"
  -- "a triumphal success"
  -- "a triumphant shout"
  
2. elated, gleeful, joyful, jubilant 
  -- full of high-spirited delight
  -- "a joyful heart"
  

`,
  lethargic: `
1. lethargic, unenergetic 
  -- deficient in alertness or activity
  -- "bullfrogs became lethargic with the first cold nights"
  

`,
  listless: `
1. listless 
  -- lacking zest or vivacity
  -- "he was listless and bored"
  
2. dispirited, listless 
  -- marked by low spirits
  -- showing no enthusiasm
  -- "a dispirited and divided Party"
  -- "reacted to the crisis with listless resignation"
  

`,
  lively: `
1. lively 
  -- full of life and energy
  -- "a lively discussion"
  -- "lively and attractive parents"
  -- "a lively party"
  
2. lively, racy 
  -- full of zest or vigor
  -- "a racy literary style"
  
3. alert, brisk, lively, merry, rattling, snappy, spanking, zippy 
  -- quick and energetic
  -- "a brisk walk in the park"
  -- "a lively gait"
  -- "a merry chase"
  -- "traveling at a rattling rate"
  -- "a snappy pace"
  -- "a spanking breeze"
  


`,
  lonely: `
1. alone, lone, lonely, solitary 
  -- lacking companions or companionship
  -- "he was alone when we met him"
  -- "she is alone much of the time"
  -- "the lone skier on the mountain"
  -- "a lonely fisherman stood on a tuft of gravel"
  -- "a lonely soul"
  -- "a solitary traveler"
  
2. lonely, lonesome 
  -- marked by dejection from being alone
  -- "felt sad and lonely"
  -- "the loneliest night of the week"
  -- "lonesome when her husband is away"
  -- "spent a lonesome hour in the bar"
  
3. lone, lonely, solitary 
  -- characterized by or preferring solitude
  -- "a lone wolf"
  -- "a lonely existence"
  -- "a man of a solitary disposition"
  -- "a solitary walk"
  

`,
  loving: `
1. loving 
  -- feeling or showing love and affection
  -- "loving parents"
  -- "loving glances"
  

`,
  melancholy: `
1. melancholy, melancholic 
  -- characterized by or causing or expressing sadness
  -- "growing more melancholy every hour"
  -- "her melancholic smile"
  -- "we acquainted him with the melancholy truth"
  
2. somber, sombre, melancholy 
  -- grave or even gloomy in character
  -- "solemn and mournful music"
  -- "a suit of somber black"
  -- "a somber mood"
  

`,
  mischievous: `
1. arch, impish, implike, mischievous, pixilated, prankish, puckish, wicked 
  -- naughtily or annoyingly playful
  -- "teasing and worrying with impish laughter"
  -- "a wicked prank"
  
2. mischievous 
  -- deliberately causing harm or damage
  -- "mischievous rumors and falsehoods"
  

`,
  miserable: `
1. miserable, suffering, wretched 
  -- very unhappy
  -- full of misery
  -- "he felt depressed and miserable"
  -- "a message of hope for suffering humanity"
  -- "wretched prisoners huddled in stinking cages"
  
2. hapless, miserable, misfortunate, pathetic, piteous, pitiable, pitiful, poor, wretched 
  -- deserving or inciting pity
  -- "a hapless victim"
  -- "miserable victims of war"
  -- "the shabby room struck her as extraordinarily pathetic"- Galsworthy
  -- "piteous appeals for help"
  -- "pitiable homeless children"
  -- "a pitiful fate"
  -- "Oh, you poor thing"
  -- "his poor distorted limbs"
  -- "a wretched life"
  
3. abject, low, low-down, miserable, scummy, scurvy 
  -- of the most contemptible kind
  -- "abject cowardice"
  -- "a low stunt to pull"
  -- "a low-down sneak"
  -- "his miserable treatment of his family"
  -- "You miserable skunk!"
  -- "a scummy rabble"
  -- "a scurvy trick"
  

`,
  nervous: `
1. nervous 
  -- easily agitated
  -- "a nervous addict"
  -- "a nervous thoroughbred"
  
2. anxious, nervous, queasy, uneasy, unquiet 
  -- causing or fraught with or showing anxiety
  -- "spent an anxious night waiting for the test results"
  -- "cast anxious glances behind her"
  -- "those nervous moments before takeoff"
  -- "an unquiet mind"
  
3. nervous, neural 
  -- of or relating to the nervous system
  -- "nervous disease"
  -- "neural disorder"
  

`,
  nurtured: `
1. foster, nurture 
  -- help develop, help grow
  -- "nurture his talents"
  
2. rear, raise, bring up, nurture, parent 
  -- bring up
  -- "raise a family"
  -- "bring up children"
  
3. nourish, nurture, sustain 
  -- provide with nourishment
  -- "We sustain ourselves on bread and water"
  -- "This kind of food is not nourishing for young children"
  

`,
  open: `
1. candid, open, heart-to-heart 
  -- openly straightforward and direct without reserve or secretiveness
  -- "his candid eyes"
  -- "an open and trusting nature"
  -- "a heart-to-heart talk"

  2. receptive, open 
  -- ready or willing to receive favorably
  -- "receptive to the proposals"
  

`,
  optimistic: `
1. optimistic 
  -- expecting the best in this best of all possible worlds
  -- "in an optimistic mood"
  -- "optimistic plans"
  -- "took an optimistic view"
  
2. affirmative, optimistic 
  -- expecting the best
  -- "an affirmative outlook"
  

`,
  mopey: ``,
  overwhelmed: `
1. overwhelm, overpower, sweep over, whelm, overcome, overtake 
  -- overcome, as with emotions or perceptual stimuli
  
2. overwhelm, deluge, flood out 
  -- charge someone with too many tasks
  
3. submerge, drown, overwhelm 
  -- cover completely or make imperceptible
  -- "I was drowned in work"
  -- "The noise drowned out her speech"
  
  

`,
  passionate: `
1. passionate 
  -- having or expressing strong emotions
  

`,
  passive: `
1. passive, inactive 
  -- lacking in energy or will
  -- "Much benevolence of the passive order may be traced to a disinclination to inflict pain upon oneself"- George Meredith
  
2. passive, peaceful 
  -- peacefully resistant in response to injustice
  -- "passive resistance"
  
3. passive 
  -- expressing that the subject of the sentence is the patient of the action denoted by the verb
  -- "academics seem to favor passive sentences"
  

`,
  perplexed: `
1. perplexed 
  -- full of difficulty or confusion or bewilderment
  -- "perplexed language"
  -- "perplexed state of the world"
  

`,
  pessimistic: `
1. pessimistic 
  -- expecting the worst possible outcome
  

`,
  proud: `
1. proud 
  -- feeling self-respect or pleasure in something by which you measure your self-worth
  -- or being a reason for pride
  -- "proud parents"
  -- "proud of his accomplishments"
  -- "a proud moment"
  -- "proud to serve his country"
  -- "a proud name"
  -- "proud princes"
  
2. gallant, lofty, majestic, proud 
  -- having or displaying great dignity or nobility
  -- "a gallant pageant"
  -- "lofty ships"
  -- "majestic cities"
  -- "proud alpine peaks"
  

`,
  puzzled: `
1. at a loss, nonplused, nonplussed, puzzled 
  -- filled with bewilderment
  -- "at a loss to understand those remarks"
  -- "puzzled that she left without saying goodbye"
  

`,
  quiet: `
1. quietly, quiet
  -- with little or no activity or no agitation
  -- "her hands rested quietly in her lap"
  -- "the rock star was quietly led out the back door"
  -- "sit here as quiet as you can"
  

`,
  radiant: `
1. beaming, beamy, effulgent, radiant, refulgent 
  -- radiating or as if radiating light
  -- "the beaming sun"
  -- "the effulgent daffodils"
  -- "a radiant sunrise"
  -- "a refulgent sunset"
  

`,
  rancorous: `
1. rancorous 
  -- showing deep-seated resentment
  -- "preserve...from rancorous envy of the rich"- Aldous Huxley
  

`,
  refreshed: `
1. fresh, invigorated, refreshed, reinvigorated 
  -- with restored energy
  

`,
  relaxed: `
1. relaxed 
  -- without strain or anxiety
  -- "gave the impression of being quite relaxed"
  -- "a relaxed and informal discussion"
  

`,
  relieved: `
1. relieved
  -- a feeling of cheerfulness or optimism that follows the removal of anxiety, pain, or distress
  -- "I breathed a sigh of relief"
  -- deliverance from or alleviation of anxiety, pain, distress, etc

`,
  reluctant: `
1. loath, loth, reluctant 
  -- unwillingness to do something contrary to your custom
  -- "a reluctant smile"
  -- "loath to admit a mistake"
  
2. reluctant 
  -- disinclined to become involved
  -- "they were usually reluctant to socialize"
  -- "reluctant to help"
  
3. reluctant 
  -- not eager
  

`,
  restless: `
1. restless, ungratified, unsatisfied 
  -- worried and uneasy
  
2. restless 
  -- ceaselessly in motion
  -- "the restless sea"
  -- "the restless wind"
  
3. restless, uneasy 
  -- lacking or not affording physical or mental rest
  -- "a restless night"
  -- "she fell into an uneasy sleep"
  

`,
  satisfied: `
1. satisfied 
  -- filled with satisfaction
  -- "a satisfied customer"
  
2. quenched, satisfied, slaked 
  -- allayed
  -- "his thirst quenched he was able to continue"
  

`,
  sensitive: `
1. sensitive 
  -- responsive to physical stimuli
  -- "a mimosa's leaves are sensitive to touch"
  -- "a sensitive voltmeter"
  -- "sensitive skin"
  -- "sensitive to light"
  
2. sensitive 
  -- being susceptible to the attitudes, feelings, or circumstances of others
  -- "sensitive to the local community and its needs"
  
3. sensible, sensitive 
  -- able to feel or perceive
  -- "even amoeba are sensible creatures"
  -- "the more sensible parts of the skin"
  

`,
  serene: `
1. calm, unagitated, serene, tranquil 
  -- not agitated
  -- without losing self-possession
  -- "spoke in a calm voice"
  -- "remained calm throughout the uproar"
  -- "he remained serene in the midst of turbulence"
  -- "a serene expression on her face"
  -- "she became more tranquil"
  -- "tranquil life in the country"
  
2. serene 
  -- completely clear and fine
  -- "serene skies and a bright blue sea"
  

`,
  shocked: `
1. aghast, appalled, dismayed, shocked 
  -- struck with fear, dread, or consternation
  

`,
  skeptical: `
1. disbelieving, skeptical, sceptical, unbelieving 
  -- denying or questioning the tenets of especially a religion
  -- "a skeptical approach to the nature of miracles"
  
2. doubting, questioning, skeptical, sceptical 
  -- marked by or given to doubt
  -- "a skeptical attitude"
  -- "a skeptical listener"
  

`,
  sleepy: `
1. sleepy, sleepy-eyed, sleepyheaded 
  -- ready to fall asleep
  -- "beginning to feel sleepy"
  -- "a sleepy-eyed child with drooping eyelids"
  -- "sleepyheaded students"
  

`,
  sorrowful: `
1. sorrowful 
  -- experiencing or marked by or expressing sorrow especially that associated with irreparable loss
  -- "sorrowful widows"
  -- "a sorrowful tale of death and despair"
  -- "sorrowful news"
  -- "even in laughter the heart is sorrowful"- Proverbs 14:13
  

`,
  startled: `
1. startled 
  -- excited by sudden surprise or alarm and making a quick involuntary movement
  -- "students startled by the teacher's quiet return"
  -- "the sudden fluttering of the startled pigeons"
  -- "her startled expression"
  

`,
  surprised: `
1. surprised 
  -- taken unawares or suddenly and feeling wonder or astonishment
  -- "surprised by her student's ingenuity"
  -- "surprised that he remembered my name"
  -- "a surprised expression"
  

`,
  suspicious: `
1. leery, mistrustful, suspicious, untrusting, wary 
  -- openly distrustful and unwilling to confide
  
2. fishy, funny, shady, suspect, suspicious 
  -- not as expected
  -- "there was something fishy about the accident"
  -- "up to some funny business"
  -- "some definitely queer goings-on"
  -- "a shady deal"
  -- "her motives were suspect"
  -- "suspicious behavior"
  

`,
  tender: `
1. tender 
  -- given to sympathy or gentleness or sentimentality
  -- "a tender heart"
  -- "a tender smile"
  -- "tender loving care"
  -- "tender memories"
  -- "a tender mother"
  
2. sensitive, sore, raw, tender 
  -- hurting
  -- "the tender spot on his jaw"

`,
  terrified: `
1. panicky, panicked, panic-stricken, panic-struck, terrified, frightened 
  -- thrown into a state of intense fear or desperation
  -- "became panicky as the snow deepened"
  -- "felt panicked before each exam"
  -- "trying to keep back the panic-stricken crowd"
  -- "the terrified horse bolted"
  

`,
  thankful: `
1. grateful, thankful 
  -- feeling or showing gratitude
  -- "a grateful heart"
  -- "grateful for the tree's shade"
  -- "a thankful smile"
  

`,
  touched: `
1. touched 
  -- having come into contact
  
2. moved, affected, stirred, touched 
  -- being excited or provoked to the expression of an emotion
  -- "too moved to speak"
  -- "very touched by the stranger's kindness"
  
3. fey, touched 
  -- slightly insane
  

`,
  tranquil: `
1. placid, quiet, still, tranquil, smooth, unruffled 
  -- free from disturbance
  -- "a ribbon of sand between the angry sea and the placid bay"
  -- "the quiet waters of a lagoon"
  -- "a lake of tranquil blue water reflecting a tranquil blue sky"
  -- "a smooth channel crossing"
  -- "scarcely a ripple on the still water"
  -- "unruffled water"
  
2. calm, unagitated, serene, tranquil 
  -- not agitated
  -- without losing self-possession
  -- "spoke in a calm voice"
  -- "remained calm throughout the uproar"
  -- "he remained serene in the midst of turbulence"
  -- "a serene expression on her face"
  -- "she became more tranquil"
  -- "tranquil life in the country"
  

`,
  troubled: `
1. troubled 
  -- characterized by or indicative of distress or affliction or danger or need
  -- "troubled areas"
  -- "fell into a troubled sleep"
  -- "a troubled expression"
  -- "troubled teenagers"
  
2. disruptive, riotous, troubled, tumultuous, turbulent 
  -- characterized by unrest or disorder or insubordination
  -- "effects of the struggle will be violent and disruptive"
  -- "riotous times"
  -- "these troubled areas"
  -- "the tumultuous years of his administration"
  -- "a turbulent and unruly childhood"
  

`,
  trusting: `
1. trustful, trusting 
  -- inclined to believe or confide readily
  -- full of trust
  -- "great brown eye, true and trustful"- Nordhoff & Hall
  

`,
  uncomfortable: `
1. uncomfortable 
  -- conducive to or feeling mental discomfort
  -- "this kind of life can prove disruptive and uncomfortable"
  -- "the uncomfortable truth"
  -- "grew uncomfortable beneath his appraising eye"
  -- "an uncomfortable way of surprising me just when I felt surest"
  -- "the teacher's presence at the conference made the child very uncomfortable"
  
2. uncomfortable 
  -- providing or experiencing physical discomfort
  -- "an uncomfortable chair"
  -- "an uncomfortable day in the hot sun"
  

`,
  uneasy: `
1. uneasy 
  -- lacking a sense of security or affording no ease or reassurance
  -- "farmers were uneasy until rain finally came"
  -- "uneasy about his health"
  -- "gave an uneasy laugh"
  -- "uneasy lies the head that wears the crown"
  -- "an uneasy coalition government"
  -- "an uneasy calm"
  -- "an uneasy silence fell on the group"
  
2. restless, uneasy 
  -- lacking or not affording physical or mental rest
  -- "a restless night"
  -- "she fell into an uneasy sleep"
  
3. anxious, nervous, queasy, uneasy, unquiet 
  -- causing or fraught with or showing anxiety
  -- "spent an anxious night waiting for the test results"
  -- "cast anxious glances behind her"
  -- "those nervous moments before takeoff"
  -- "an unquiet mind"

  

`,
  unglued: `
`,
  unhappy: `
1. unhappy 
  -- experiencing or marked by or causing sadness or sorrow or discontent
  -- "unhappy over her departure"
  -- "unhappy with her raise"
  -- "after the argument they lapsed into an unhappy silence"
  -- "had an unhappy time at school"
  -- "the unhappy (or sad) news"
  -- "he looks so sad"
  
2. dysphoric, distressed, unhappy 
  -- generalized feeling of distress
  

`,
  unsteady: `
1. unsteady 
  -- subject to change or variation
  -- "her unsteady walk"
  -- "his hand was unsteady as he poured the wine"
  -- "an unsteady voice"
  
2. unfirm, unsteady 
  -- not firmly or solidly positioned
  -- "climbing carefully up the unsteady ladder"
  -- "an unfirm stance"
  

`,
  violent: `
1. violent 
  -- acting with or marked by or resulting from great force or energy or emotional intensity
  -- "a violent attack"
  -- "a violent person"
  -- "violent feelings"
  -- "a violent rage"
  -- "felt a violent dislike"

`,
  warm: `
1. warm 
  -- psychologically warm
  -- friendly and responsive
  -- "a warm greeting"
  -- "a warm personality"
  -- "warm support"
  
2. affectionate, fond, lovesome, tender, warm 
  -- having or displaying warmth or affection
  -- "affectionate children"
  -- "a fond embrace"
  -- "fond of his nephew"
  -- "a tender glance"
  -- "a warm embrace"


`,
  weary: `
1. tired, weary, heavy, exhausted
  -- physically and mentally fatigued

`,
  withdrawn: `
1. recluse, reclusive, withdrawn 
  -- withdrawn from society
  -- seeking solitude
  -- "lived an unsocial reclusive life"
  
2. indrawn, withdrawn 
  -- tending to reserve or introspection
  -- "a quiet indrawn person"
  

`,
  worried: `
1. disquieted, distressed, disturbed, upset, worried 
  -- afflicted with or marked by anxious uneasiness or trouble or grief
  -- "too upset to say anything"
  -- "spent many disquieted moments"
  -- "distressed about her son's leaving home"
  -- "lapsed into disturbed sleep"
  -- "worried parents"
  -- "a worried frown"
  -- "one last worried check of the sleeping children"
  
2. apprehensive, worried 
  -- mentally upset over possible misfortune or danger etc
  -- "apprehensive about her job"
  -- "not used to a city and worried about small things"
  -- "felt apprehensive about the consequences"
  

`,
  zestful: `
1. zestful, yeasty, zesty, barmy 
  -- marked by spirited enjoyment`,
};

export function getFeelingDefinition(name: string) {
  return (FeelingDefinitions as any)[name] || "Definition needed";
}
