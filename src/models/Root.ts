import { Instance, onSnapshot, types } from "mobx-state-tree";
import { createContext, useContext } from "react";
import { log } from "../components/log";
import {configData, ConfigDataModel} from "./Data"
import { CheckInModel } from "./CheckIn"

const RootModel = types
  .model({
    loading: false,
    config: ConfigDataModel,
    checkIn: CheckInModel
  })
  .actions((self) => ({
    setLoading: (b: boolean) => {
      self.loading = b;
    },
  }))
  .actions((self) => ({
    load: () => {
      // self.setLoading(true)
      // self.rootFlowScenes.load().finally(() => self.setLoading(false))
    },
  }));

const staticData = {config: {data: configData}}
let initialState = RootModel.create({
  ...staticData,
  checkIn: {needs: {}, feelings: []}
});

if ((process as any).browser) {
  const data = localStorage.getItem("rootState");
  if (data) {
    const json = JSON.parse(data);
    if (RootModel.is(json)) {
      initialState = RootModel.create({...json, ...staticData} as any);
    }
  }
}

export const rootStore = initialState;

onSnapshot(rootStore, (snapshot) => {
  log("Snapshot: ", snapshot);
  localStorage.setItem("rootState", JSON.stringify(snapshot));
});

export type RootInstance = Instance<typeof RootModel>;
const RootStoreContext = createContext<null | RootInstance>(null);

export const Provider = RootStoreContext.Provider;
export function useMst() {
  const store = useContext(RootStoreContext);
  if (store === null) {
    throw new Error("Store cannot be null, please add a context provider");
  }
  (window as any).___nvcstore = store
  return store;
}
