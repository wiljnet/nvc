import { types } from "mobx-state-tree";
import { primitiveObject } from "./modelutil"

export type FeelingDefinition = {
    name: string,
    definition: string,
}

export type Feeling = {
  name: string,
  pleasant: boolean,
  children: string[]
}

export type Need = {
  name: string,
  children?: Need[]
}

export const needRangeValues = [
    "no",
    "neutral",
    "yes"
]

export const pleasantFeelings : Feeling[] = [
    {
        name: 'Peaceful',
        pleasant: true,
        children: [
            'absorbed',
            'blissful',
            'calm',
            'carefree',
            'composed',
            'content',
            'engrossed',
            'expansive',
            'fulfilled',
            'loving',
            'quiet',
            'relaxed',
            'relieved',
            'satisfied',
            'serene',
            'tranquil',
        ],
    },
    {
        name: 'Loving',
        pleasant: true,
        children: [
            'adoring',
            'affectionate',
            'amorous',
            'appreciative',
            'compassionate',
            'friendly',
            'grateful',
            'nurtured',
            'open',
            'passionate',
            'radiant',
            'tender',
            'thankful',
            'trusting',
            'warm',
        ],
    },
  
    {
        name: 'Glad',
        pleasant: true,
        children: [
            'confident',
            'delighted',
            'ecstatic',
            'encouraged',
            'excited',
            'exhilarated',
            'glorious',
            'grateful',
            'happy',
            'hopeful',
            'inspired',
            'joyful',
            'optimistic',
            'proud',
            'satisfied',
            'touched',
        ],
    },
  
    {
        name: 'Playful',
        pleasant: true,
        children: [
            'adventurous',
            'alive',
            'buoyant',
            'effervescent',
            'electrified',
            'energetic',
            'exuberant',
            'giddy',
            'goofy',
            'impish',
            'invigorated',
            'jubilant',
            'lively',
            'mischievous',
            'refreshed',
            'zestful',
        ],
    },
  
    {
        name: 'Interested',
        pleasant: true,
        children: [
            'absorbed',
            'alert',
            'aroused',
            'astonished',
            'curious',
            'eager',
            'enriched',
            'enthusiastic',
            'fascinated',
            'helpful',
            'inquisitive',
            'intense',
            'intrigued',
            'involved',
            'surprised',
        ],
    }
]


export const unpleasantFeelings : Feeling[] = [
  {
      name: 'Mad',
      pleasant: false,
      children: [
          'agitated',
          'angry',
          'animosity',
          'bitter',
          'cantankerous',
          'disgruntled',
          'disgusted',
          'edgy',
          'enraged',
          'exasperated',
          'frustrated',
          'furious',
          'grouchy',
          'hostile',
          'impatient',
          'irate',
          'irked',
          'irritable',
          'pessimistic',
          'rancorous',
          'violent',
      ],
  },

  {
      name: 'Sad',
      pleasant: false,
      children: [
          'blue',
          'dejected',
          'depressed',
          'despairing',
          'despondent',
          'discouraged',
          'disheartened',
          'dismayed',
          'distant',
          'distressed',
          'gloomy',
          'heavy',
          'helpless',
          'lonely',
          'melancholy',
          'miserable',
          'overwhelmed',
          'sorrowful',
          'troubled',
          'unhappy',
      ],
  },

  {
      name: 'Scared',
      pleasant: false,
      children: [
          'afraid',
          'anguished',
          'anxious',
          'apprehensive',
          'concerned',
          'desperate',
          'dread',
          'fearful',
          'frightened',
          'horrified',
          'insecure',
          'jealous',
          'jittery',
          'lonely',
          'nervous',
          'sensitive',
          'shocked',
          'startled',
          'suspicious',
          'terrified',
          'worried',
      ],
  },

  {
      name: 'Tired',
      pleasant: false,
      children: [
          'blah',
          'bored',
          'comatose',
          'disinterested',
          'dull',
          'exhausted',
          'fatigued',
          'fidgety',
          'heavy',
          'helpless',
          'indifferent',
          'inert',
          'lethargic',
          'listless',
          'mopey',
          'overwhelmed',
          'passive',
          'reluctant',
          'sleepy',
          'weary',
      ],
  },

  {
      name: 'Confused',
      pleasant: false,
      children: [
          'apathetic',
          'boggled',
          'chagrined',
          'detached',
          'embarrassed',
          'frustrated',
          'hesitant',
          'hurt',
          'irritated',
          'perplexed',
          'puzzled',
          'restless',
          'skeptical',
          'suspicious',
          'troubled',
          'uncomfortable',
          'uneasy',
          'unglued',
          'unsteady',
          'withdrawn',
      ],
  },
]

export const feelings = [...pleasantFeelings, ...unpleasantFeelings]

function flattenFeelingNames(input: Feeling[]) : string[] {
    const result : string[] = []
    input.forEach(x => {
        result.push(x.name.toLowerCase())
        x.children.forEach(c => result.push(c.toLowerCase()))
    })
    return result
}

export const pleasantFeelingNames = flattenFeelingNames(pleasantFeelings)
export const unpleasantFeelingNames = flattenFeelingNames(unpleasantFeelings)

export const allFeelingNames = [...pleasantFeelingNames, ...unpleasantFeelingNames]

export const needs : Need[] = [
  {
      name: 'Well Being',
      children: [
          {
              name: 'Safety',
              children: [
                  { name: 'peace' },
                  { name: 'confidence' },
                  { name: 'comfort' },
                  { name: 'consistency' },
                  { name: 'courage' },
                  { name: 'equanimity' },
                  { name: 'order' },
                  { name: 'predictability' },
                  { name: 'protection from harm' },
                  { name: 'security' },
                  { name: 'stability' },
                  { name: 'shelter' },
                  { name: 'trust' },
                  { name: 'reliability' },
              ],
          },

          {
              name: 'Health',
              children: [
                  { name: 'abundance' },
                  { name: 'balance' },
                  { name: 'exercise' },
                  { name: 'movement' },
                  { name: 'flexibility' },
                  { name: 'food' },
                  { name: 'water' },
                  { name: 'air' },
                  { name: 'shelter' },
                  { name: 'nutrition' },
                  { name: 'nourishment' },
                  { name: 'prosperity' },
                  { name: 'richness' },
                  { name: 'simplicity' },
                  { name: 'sleep' },
                  { name: 'sustainability' },
                  { name: 'wellness' },
              ],
          },

          {
              name: 'Play',
              children: [
                  { name: 'beauty' },
                  { name: 'calm' },
                  { name: 'ease' },
                  { name: 'flow' },
                  { name: 'energy' },
                  { name: 'vitality' },
                  { name: 'enthusiasm' },
                  { name: 'exuberance' },
                  { name: 'fun' },
                  { name: 'joy' },
                  { name: 'happiness' },
                  { name: 'harmony' },
                  { name: 'humor' },
                  { name: 'recreation' },
                  { name: 'rejuvenation' },
                  { name: 'rest' },
                  { name: 'relaxation' },
                  { name: 'tranquility' },
                  { name: 'serenity' },
              ],
          },
      ],
  },

  {
      name: 'Connection',
      children: [
          {
              name: 'Care',
              children: [
                  { name: 'acceptance' },
                  { name: 'affection' },
                  { name: 'appreciation' },
                  { name: 'fairness' },
                  { name: 'justice' },
                  { name: 'generosity' },
                  { name: 'intimacy' },
                  { name: 'closeness' },
                  { name: 'kindness' },
                  { name: 'love' },
                  { name: 'mattering' },
                  { name: 'importance' },
                  { name: 'nurturing' },
                  { name: 'unconditional positive regard' },
                  { name: 'valuing' },
                  { name: 'prizing' },
                  { name: 'dignity' },
                  { name: 'warmth' },
                  { name: 'touch' },
              ],
          },

          {
              name: 'Empathy',
              children: [
                  { name: 'awareness' },
                  { name: 'acknowledgment' },
                  { name: 'communication' },
                  { name: 'compassion' },
                  { name: 'consideration' },
                  { name: 'presence' },
                  { name: 'recognition' },
                  { name: 'receptivity' },
                  { name: 'seeing' },
                  { name: 'being seen' },
                  { name: 'sensitivity' },
                  { name: 'understanding' },
              ],
          },

          {
              name: 'Community',
              children: [
                  { name: 'belonging' },
                  { name: 'companionship' },
                  { name: 'collaboration' },
                  { name: 'cooperation' },
                  { name: 'friendship' },
                  { name: 'fellowship' },
                  { name: 'help' },
                  { name: 'support' },
                  { name: 'inclusion' },
                  { name: 'equality' },
                  { name: 'interdependence' },
                  { name: 'home' },
                  { name: 'hospitality' },
                  { name: 'welcoming' },
                  { name: 'mutuality' },
                  { name: 'reciprocity' },
                  { name: 'partnership' },
                  { name: 'synergy' },
              ],
          },
      ],
  },

  {
      name: 'Self Expression',
      children: [
          {
              name: 'Freedom',
              children: [
                  { name: 'adventure' },
                  { name: 'self-actualization' },
                  { name: 'aliveness' },
                  { name: 'autonomy' },
                  { name: 'choice' },
                  { name: 'courage' },
                  { name: 'creativity' },
                  { name: 'fun' },
                  { name: 'growth' },
                  { name: 'independence' },
                  { name: 'innovation' },
                  { name: 'joy' },
                  { name: 'happiness' },
                  { name: 'spontaneity' },
                  { name: 'wonder' },
                  { name: 'discovery' },
              ],
          },

          {
              name: 'Honesty',
              children: [
                  { name: 'authenticity' },
                  { name: 'congruence' },
                  { name: 'dependability' },
                  { name: 'integrity' },
                  { name: 'trust' },
                  { name: 'openness' },
                  { name: 'power' },
                  { name: 'empowerment' },
                  { name: 'presence' },
                  { name: 'realness' },
                  { name: 'reliability' },
                  { name: 'respect' },
                  { name: 'honoring' },
              ],
          },

          {
              name: 'Meaning',
              children: [
                  { name: 'achievement' },
                  { name: 'success' },
                  { name: 'appreciation' },
                  { name: 'gratitude' },
                  { name: 'celebration' },
                  { name: 'mourning' },
                  { name: 'challenge' },
                  { name: 'contribution' },
                  { name: 'efficacy' },
                  { name: 'effectiveness' },
                  { name: 'excellence' },
                  { name: 'mastery' },
                  { name: 'skill' },
                  { name: 'inspiration' },
                  { name: 'learning' },
                  { name: 'focus' },
                  { name: 'passion' },
                  { name: 'commitment' },
                  { name: 'purpose' },
                  { name: 'structure' },
                  { name: 'discipline' },
                  { name: 'vision' },
                  { name: 'clarity' },
                  { name: 'wisdom' },
              ],
          },
      ],
  },
]

export const configData = { needRangeValues, pleasantFeelings, unpleasantFeelings, feelings, pleasantFeelingNames, unpleasantFeelingNames, allFeelingNames, needs }
export const ConfigDataPrimitive = primitiveObject<typeof configData>("ConfigData")


export const ConfigDataModel = types
  .model({
    data: ConfigDataPrimitive
  });
