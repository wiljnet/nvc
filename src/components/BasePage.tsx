import {
  IonContent,
  IonHeader,
  IonIcon,
  IonLabel,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { codeOutline } from "ionicons/icons";
import "./BasePage.css";

const GITPOD_MESSAGE = "Running on Gitpod.io";

const GitpodBadge: React.FC = () => {
  return (
    <>
      {process.env.REACT_APP_IS_GITPOD && (
        <IonLabel slot="end" title={GITPOD_MESSAGE}>
          <IonIcon
            style={{ pointerEvents: "none" }}
            slot="end"
            icon={codeOutline}
          />
        </IonLabel>
      )}
    </>
  );
};

const BasePage: React.FC<{ title: string }> = ({ title, children }) => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle slot="start">
            <IonText color="primary">{title}</IonText>
          </IonTitle>
          <GitpodBadge />
        </IonToolbar>
      </IonHeader>
      <IonContent>{children}</IonContent>
    </IonPage>
  );
};

export default BasePage;
