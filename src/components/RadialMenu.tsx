import { IonFab, IonFabButton, IonFabList, IonIcon } from "@ionic/react";
import { closeOutline, menuOutline } from "ionicons/icons";
import { useState } from "react";

import "./RadialMenu.css";

export type ClickHandler = (name: string) => void
export type RadialMenuSlot = "left" | "center" | "right"

export const RadialMenuItem: React.FC<{title: string}> = ({title, children}) => {
  return (<a className="menu-item">{title}</a>)
}

export const RadialMenu: React.FC<{name: string, active: boolean, onClick: ClickHandler, title?: string, slot: RadialMenuSlot}> = ({name, active, onClick, title, slot, children}) => {
  const menuIcon = <IonIcon icon={closeOutline} />
  console.log(`children`, (children as any).map((x:any) => x.key) )
  const onItemClick = (e:any) => console.log(`clicked`, e)
  return (     <div
    className={`circular-menu circular-menu-${slot} ${active && "active"}`}
  >
    <a className="floating-btn" onClick={() => onClick(name)}>
      {active && slot === "left" && menuIcon}
      {title || name}
      {active && slot === "right" && menuIcon}
    </a>

    <menu className="items-wrapper">
    { children }
    </menu>
  </div>
)
}

export const RadialMenus: React.FC = () => {
  const [activeMenu, setActiveMenu] = useState("feelings");
  const toggleMenu = (name:string) => setActiveMenu(name === activeMenu ? "" : name)
  return (
    <>
    <RadialMenu name="needs" active={activeMenu === "needs"} onClick={toggleMenu} slot="right" title="Needs">
      {['Well Being', 'Connection', 'Self Expression'].map(x => <RadialMenuItem key={x} title={x} />)}
    </RadialMenu>
      
    <RadialMenu name="feelings" active={activeMenu === "feelings"} onClick={toggleMenu} slot="left" title="Feelings">
      {['Peaceful', 'Loving', 'Glad', 'Playful', 'Interested', 'Mad', 'Sad', 'Scared', 'Tired', 'Confused'].map(x => <RadialMenuItem key={x} title={x} />)}
    </RadialMenu>
    </>
  );
};
