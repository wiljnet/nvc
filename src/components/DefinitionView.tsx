import {
  IonButton,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonIcon,
  IonPage,
  IonRow,
  IonText,
  IonTextarea,
  IonTitle,
  IonToast,
  IonToolbar,
} from "@ionic/react";
import { volumeHighOutline } from "ionicons/icons";
import React, { useCallback, useEffect, useState } from "react";

const canUseSpeech = SpeechSynthesisUtterance !== undefined;

type DefinitionViewProps = {
  word: string;
  definition: string;
  backLink: string;
};

const DefinitionView: React.FC<DefinitionViewProps> = ({
  word,
  definition,
  backLink,
}) => {

  const sayWord = useCallback(() => {
    let utterance = new SpeechSynthesisUtterance(word);
    speechSynthesis.speak(utterance);
  }, [word]);


  const [showToast, setShowToast] = useState(false);
  const dismissToast = useCallback(() => setShowToast(false), []);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle slot="start">
            <IonText color="primary">{word}</IonText>
          </IonTitle>
          {canUseSpeech && (
            <IonButton slot="end" size="small" onClick={sayWord}>
              <IonIcon
                icon={volumeHighOutline}
                className="hear-it-button-icon"
              />
              <IonText>hear it</IonText>
            </IonButton>
          )}
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>

          <>
            <IonRow>
              <IonCol>
                <pre className="definition-text">{definition}</pre>
              </IonCol>
            </IonRow>
            <IonRow className="ion-align-items-center">
              <IonCol className="ion-text-center">
                <IonButton routerLink={backLink}>Back</IonButton>
              </IonCol>
            </IonRow>
          </>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default DefinitionView;
