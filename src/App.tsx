import {
  IonApp,
  IonIcon,
  IonLabel,
  IonLoading,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";
import "@ionic/react/css/display.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/float-elements.css";
/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/typography.css";
import {
  barChartOutline,
  heartOutline,
  helpCircleOutline,
  listOutline,
} from "ionicons/icons";
import { observer } from "mobx-react-lite";
import { lazy, Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import "./App.css";
import { useMst } from "./models/Root";
/* Theme variables */
import "./theme/variables.css";

const AboutPage = lazy(() => import(`./pages/About`));
const CheckInPage = lazy(() => import(`./pages/CheckIn`));
const FeelingsPage = lazy(() => import(`./pages/Feelings`));
const NeedsPage = lazy(() => import(`./pages/Needs`));

setupIonicReact({
  mode: "md",
});

const App: React.FC = observer(() => {
  const { loading } = useMst();

  return (
    <IonApp>
      <IonLoading isOpen={loading} message={"Loading..."} />
      <IonReactRouter>
        <Suspense fallback={<IonLoading isOpen={true} />}>
          <IonTabs>
            <IonRouterOutlet id="main">
              <Switch>
                <Route path="/feelings">
                  <FeelingsPage />
                </Route>
                <Route path="/needs">
                  <NeedsPage />
                </Route>
                <Route exact path="/about">
                  <AboutPage />
                </Route>
                <Route exact path="/checkin">
                  <CheckInPage />
                </Route>
                <Route path="/">
                  <Redirect to="/feelings" />
                </Route>
              </Switch>
            </IonRouterOutlet>
            <IonTabBar slot="bottom">
              <IonTabButton tab="feelings" href="/feelings">
                <IonIcon icon={heartOutline} />
                <IonLabel>Feelings</IonLabel>
              </IonTabButton>
              <IonTabButton tab="needs" href="/needs">
                <IonIcon icon={listOutline} />
                <IonLabel>Needs</IonLabel>
              </IonTabButton>
              <IonTabButton tab="checkin" href="/checkin">
                <IonIcon icon={barChartOutline} />
                <IonLabel>Check In</IonLabel>
              </IonTabButton>
              <IonTabButton tab="about" href="/about">
                <IonIcon icon={helpCircleOutline} />
                <IonLabel>About</IonLabel>
              </IonTabButton>
            </IonTabBar>
          </IonTabs>
        </Suspense>
      </IonReactRouter>
    </IonApp>
  );
});

export default App;
