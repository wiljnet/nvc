import {
  IonActionSheet,
  IonBadge,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonLabel,
  IonPage,
  IonRow,
  IonSegment,
  IonSegmentButton,
  IonText,
  IonTitle,
  IonToast,
  IonToolbar,
  SegmentChangeEventDetail,
} from "@ionic/react";
import { closeOutline } from "ionicons/icons";
import { observer } from "mobx-react-lite";
import React, { useCallback, useEffect, useState } from "react";
import { RadialMenus } from "../components/RadialMenu";
import useClipboard from "../hooks/useClipboard";
import { Feeling, Need } from "../models/Data";
import { useMst } from "../models/Root";
import "./CheckIn.css";

type CommonProps = {
  onNeedClicked: (e: any) => void;
  setFeeling: (e: any) => void;
};
function extractCommonProps(props: CommonProps) {
  const { onNeedClicked, setFeeling } = props;
  return { onNeedClicked, setFeeling };
}
const blankWord = `____`;

const BadgePhrase: React.FC<{
  values: string[];
  color: string;
  onClick: (e: any) => void;
}> = ({ values, color, onClick }) => {
  const arr = values.length === 0 ? [blankWord] : values;

  return (
    <>
      {arr.map((x, i, arr) => {
        const isOnly = arr.length == 1;
        const isLast = !isOnly && i === arr.length - 1;
        const isNotLast = !isOnly && !isLast;

        return (
          <React.Fragment key={x || ``}>
            {isLast && ` and `}
            <IonBadge
              className="sentence-word"
              data-id={x}
              onClick={onClick}
              color={color}
            >
              {x}
            </IonBadge>
            {isNotLast && `, `}
          </React.Fragment>
        );
      })}
    </>
  );
};

const ConnectingSentence: React.FC<
  {
    feelingNames: string[];
    needNames: string[];
    pleasant: boolean;
  } & CommonProps
> = (props) => {
  const { feelingNames, needNames, pleasant, setFeeling, onNeedClicked } =
    props;
  const commonProps = extractCommonProps(props);
  const color = pleasant ? "success" : "danger";
  const feelingString = `I might be feeling `;
  const connectingString = ` because my need${
    needNames.length > 1 ? "s" : ""
  } for `;
  const metString = ` ${needNames.length > 1 ? "are" : "is"} ${
    pleasant ? "" : "not"
  } being met.`;
  return (
    <IonText
      id={`${pleasant ? "pleasant" : "unpleasant"}-feelings-sentence`}
      className="checkin-feelings-sentence"
    >
      {feelingString}
      <BadgePhrase values={feelingNames} color={color} onClick={setFeeling} />
      {connectingString}
      <BadgePhrase values={needNames} color={color} onClick={onNeedClicked} />
      {metString}
    </IonText>
  );
};
const ConnectingParagraph: React.FC<CommonProps> = observer((props) => {
  const commonProps = extractCommonProps(props);

  const { checkIn } = useMst();
  const isBlank =
    checkIn.feelings.length === 0 &&
    checkIn.metNeeds.length === 0 &&
    checkIn.unmetNeeds.length === 0;

  const selectedPleasant =
    checkIn.pleasantFeelings.length + checkIn.metNeeds.length > 0;

  const selectedUnpleasant =
    checkIn.unpleasantFeelings.length + checkIn.unmetNeeds.length > 0;

  return (
    <IonCard>
      <IonCardContent>
        {isBlank && (
          <ConnectingSentence
            feelingNames={checkIn.pleasantFeelings}
            needNames={checkIn.metNeeds}
            pleasant={true}
            {...commonProps}
          />
        )}
        {selectedPleasant && (
          <ConnectingSentence
            feelingNames={checkIn.pleasantFeelings}
            needNames={checkIn.metNeeds}
            pleasant={true}
            {...commonProps}
          />
        )}
        {selectedUnpleasant && (
          <>
            <br />
            <ConnectingSentence
              feelingNames={checkIn.unpleasantFeelings}
              needNames={checkIn.unmetNeeds}
              pleasant={false}
              {...commonProps}
            />
          </>
        )}
      </IonCardContent>
    </IonCard>
  );
});

const FeelingsBlock: React.FC<
  {
    feelings: Feeling[];
    isPleasant: boolean;
  } & CommonProps
> = observer((props) => {
  const { feelings, setFeeling, isPleasant } = props;
  const { checkIn } = useMst();

  return (
    <IonRow>
      {feelings.map(({ name, children }) => {
        return (
          <IonCol
            key={name}
            sizeXs="12"
            sizeSm="12"
            sizeMd="6"
            sizeLg="4"
            sizeXl="2"
          >
            <IonCard>
              <IonCardHeader>
                <IonCardTitle>{name}</IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
                <IonText className="checkin-feelings-list-words">
                  {children.map<React.ReactNode>((word) => {
                    const value = checkIn.feelings.includes(word);
                    const color = value
                      ? isPleasant
                        ? "success"
                        : "danger"
                      : "medium";

                    return (
                      <IonBadge
                        className="wordlist-word"
                        key={word}
                        data-id={word}
                        onClick={setFeeling}
                        color={color}
                      >
                        {word}
                      </IonBadge>
                    );
                  })}
                </IonText>
              </IonCardContent>
            </IonCard>
          </IonCol>
        );
      })}
    </IonRow>
  );
});

const NeedsBlock: React.FC<
  {
    needs: Need[];
  } & CommonProps
> = observer(({ needs, onNeedClicked }) => {
  const { checkIn } = useMst();
  return (
    <IonRow>
      {needs.map(({ name, children }) => {
        return (
          <IonCol
            key={name}
            sizeXs="12"
            sizeSm="12"
            sizeMd="4"
            sizeLg="4"
            sizeXl="4"
          >
            <IonCard>
              <IonCardHeader>
                <IonCardTitle>{name}</IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
                <IonText className="checkin-needs-list-words">
                  {children?.map<React.ReactNode>(({ name: word }) => {
                    const value = checkIn.needs.get(word)?.value || 0;

                    return (
                      <IonBadge
                        className="wordlist-word"
                        key={word}
                        data-id={word}
                        onClick={onNeedClicked}
                        color={
                          value === 0
                            ? "medium"
                            : value > 0
                            ? "success"
                            : "danger"
                        }
                      >
                        {word}
                      </IonBadge>
                    );
                  })}
                </IonText>
              </IonCardContent>
            </IonCard>
          </IonCol>
        );
      })}
    </IonRow>
  );
});

function tryVibrate(milliseconds: number) {
  try {
    window.navigator.vibrate && window.navigator.vibrate(milliseconds);
  } catch (e) {
    console.error();
  }
}
function shortVibrate() {
  tryVibrate(25);
}

const CheckInPage: React.FC = observer(() => {
  const { config, checkIn } = useMst();
  const data = config.data!;

  const [showing, setShowing] = useState<string>("feelings");
  const [checkinText, setCheckinText] = useState<string>("");

  const [isCopied, setCopied] = useClipboard(checkinText, {
    successDuration: 1000,
  });
  const [showCopiedToast, setShowCopiedToast] = useState(false);
  const [showClearedToast, setShowClearedToast] = useState(false);
  const [needPickerValue, setNeedPickerValue] = useState("");

  const setFeeling = useCallback(
    (e: any) => {
      const id = e.target.dataset.id;
      if (id === blankWord) {
        return;
      }
      const checked = checkIn.feelings.includes(id);
      checkIn.setFeeling(id, !checked);
      shortVibrate();
    },
    [checkIn]
  );

  const setNeedMet = useCallback(() => {
    checkIn.setNeed(needPickerValue, 1);
    shortVibrate();
  }, [checkIn, needPickerValue]);

  const setNeedUnmet = useCallback(() => {
    checkIn.setNeed(needPickerValue, -1);
    shortVibrate();
  }, [checkIn, needPickerValue]);

  const setNeedNeutral = useCallback(() => {
    checkIn.setNeed(needPickerValue, -0);
    shortVibrate();
  }, [checkIn, needPickerValue]);

  const onNeedClicked = useCallback(
    (e: any) => {
      const id = e.target.dataset.id;
      if (id === blankWord) {
        return;
      }
      setNeedPickerValue(id);
    },
    [checkIn]
  );

  const onSegmentChange = useCallback(
    (e: CustomEvent<SegmentChangeEventDetail>) => {
      if (e.detail.value === "clear") {
        checkIn.clear();
        setShowClearedToast(true);
        shortVibrate();
      } else if (e.detail.value === "copy") {
        const getSentence = (x: string) =>
          document?.getElementById(`${x}-feelings-sentence`)?.innerText;
        const sentences = [
          getSentence("pleasant"),
          getSentence("unpleasant"),
        ].filter(Boolean);

        setCheckinText(sentences.join("\n\n"));
        shortVibrate();
      } else {
        setShowing(e.detail.value || ``);
      }
    },
    []
  );

  const dismissCopiedToast = useCallback(() => setShowCopiedToast(false), []);
  const dismissClearedToast = useCallback(() => setShowClearedToast(false), []);
  const dismissNeedActionSheet = useCallback(() => setNeedPickerValue(""), []);

  useEffect(() => {
    if (isCopied) {
      setShowCopiedToast(true);
    }
  }, [isCopied, setShowCopiedToast]);

  useEffect(() => {
    if (checkinText) {
      setCopied();
      setCheckinText("");
    }
  }, [checkinText]);
  const commonProps: CommonProps = { setFeeling, onNeedClicked };
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle slot="start">
            <IonText color="primary">Check In</IonText>
          </IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonActionSheet
        isOpen={needPickerValue !== ""}
        header={`My need for ${needPickerValue} is`}
        onDidDismiss={dismissNeedActionSheet}
        buttons={[
          {
            text: `Met`,
            data: `Met`,
            handler: setNeedMet,
          },
          {
            text: `Neutral`,
            data: `Neutral`,
            handler: setNeedNeutral,
          },
          {
            text: `Unmet`,
            data: `Unmet`,
            handler: setNeedUnmet,
          },
          {
            text: "Cancel",
            icon: closeOutline,
            role: "cancel",
          },
        ]}
      ></IonActionSheet>
      <ConnectingParagraph {...commonProps} />

      <IonContent>
        <IonGrid>
          {showing === "feelings" && (
            <>
              <IonRow>
                <IonCol>
                  <h5>Pleasant feelings</h5>
                </IonCol>
              </IonRow>
              <FeelingsBlock
                feelings={data.pleasantFeelings}
                isPleasant={true}
                {...commonProps}
              />
              <IonRow>
                <IonCol>
                  <h5>Unpleasant feelings</h5>
                </IonCol>
              </IonRow>
              <FeelingsBlock
                feelings={data.unpleasantFeelings}
                isPleasant={false}
                {...commonProps}
              />
            </>
          )}
          {showing === "needs" && (
            <>
              {data?.needs.map(({ name, children }) => {
                return (
                  <React.Fragment key={name}>
                    <IonRow>
                      <IonCol>
                        <h5>{name}</h5>
                      </IonCol>
                    </IonRow>
                    {children?.length && (
                      <NeedsBlock needs={children} {...commonProps} />
                    )}
                  </React.Fragment>
                );
              })}
            </>
          )}
        </IonGrid>
      </IonContent>

      <IonSegment value={showing} onIonChange={onSegmentChange}>
        <IonSegmentButton value="feelings">
          <IonLabel>+ feelings</IonLabel>
        </IonSegmentButton>
        <IonSegmentButton value="needs">
          <IonLabel>+ needs</IonLabel>
        </IonSegmentButton>
        <IonSegmentButton value="copy">
          <IonLabel>Copy</IonLabel>
        </IonSegmentButton>
        <IonSegmentButton value="clear">
          <IonLabel>Clear</IonLabel>
        </IonSegmentButton>
      </IonSegment>
      {false && <RadialMenus />}
      <IonToast
        isOpen={showCopiedToast}
        onDidDismiss={dismissCopiedToast}
        message="Copied to clipboard"
        duration={500}
        position="middle"
      />
      <IonToast
        isOpen={showClearedToast}
        onDidDismiss={dismissClearedToast}
        message="Cleared"
        duration={500}
        position="middle"
      />
    </IonPage>
  );
});

export default CheckInPage;
