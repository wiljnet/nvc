import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonCol,
  IonGrid,
  IonRouterLink,
  IonRow,
  IonText,
} from "@ionic/react";
import { observer } from "mobx-react-lite";
import React, { lazy } from "react";
import { Route, Switch } from "react-router-dom";
import BasePage from "../components/BasePage";
import { useMst } from "../models/Root";
import "./Needs.css";

const DefinitionPage = lazy(() => import(`./NeedDefinition`));

const NeedsList: React.FC = observer(() => {
  const { config } = useMst();
  const data = config.data!;
  return (
    <BasePage title="Universal human needs/values">
      <IonGrid>
        {data.needs.map(({ name: topName, children: topChildren }) => {
          return (
            <React.Fragment key={topName}>
              <IonRow>
                <IonCol>
                  <h4>{topName}</h4>
                </IonCol>
              </IonRow>

              <IonRow>
                {topChildren?.map(
                  ({ name: middleName, children: middleChildren }) => (
                    <IonCol
                      key={middleName}
                      sizeXs="12"
                      sizeSm="12"
                      sizeMd="4"
                      sizeLg="4"
                      sizeXl="4"
                    >
                      <IonCard key={middleName}>
                        <IonCardHeader>
                          <IonCardTitle>{middleName}</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                          <IonText className="needs-list-words">
                            {middleChildren
                              ?.map<React.ReactNode>(({ name }) => (
                                <IonRouterLink
                                  key={name}
                                  routerLink={`/needs/${encodeURIComponent(
                                    name
                                  )}`}
                                >
                                  {name}
                                </IonRouterLink>
                              ))
                              .reduce((prev, curr) => [prev, ", ", curr])}
                          </IonText>
                        </IonCardContent>
                      </IonCard>
                    </IonCol>
                  )
                )}
              </IonRow>
            </React.Fragment>
          );
        })}
      </IonGrid>
    </BasePage>
  );
});

const NeedsPage: React.FC = () => {
  return (
    <Switch>
      <Route exact path="/needs/:word">
        <DefinitionPage />
      </Route>
      <Route path="/needs">
        <NeedsList />
      </Route>
    </Switch>
  );
};

export default NeedsPage;
