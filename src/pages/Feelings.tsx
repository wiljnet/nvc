import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonCol,
  IonGrid,
  IonRouterLink,
  IonRow,
  IonText,
} from "@ionic/react";
import { observer } from "mobx-react-lite";
import React, { lazy } from "react";
import { Route, Switch } from "react-router-dom";
import BasePage from "../components/BasePage";
import { Feeling } from "../models/Data";
import { useMst } from "../models/Root";
import "./Feelings.css";

const DefinitionPage = lazy(() => import(`./FeelingDefinition`));

const FeelingsBlock: React.FC<{
  feelings: Feeling[];
}> = ({ feelings }) => {
  return (
    <IonRow>
      {feelings.map(({ name, children }) => {
        return (
          <IonCol
            key={name}
            sizeXs="12"
            sizeSm="12"
            sizeMd="6"
            sizeLg="4"
            sizeXl="2"
          >
            <IonCard>
              <IonCardHeader>
                <IonCardTitle>{name}</IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
                <IonText className="feelings-list-words">
                  {children
                    .map<React.ReactNode>((word) => (
                      <IonRouterLink
                        key={word}
                        routerLink={`/feelings/${word}`}
                      >
                        {word}
                      </IonRouterLink>
                    ))
                    .reduce((prev, curr) => [prev, ", ", curr])}
                </IonText>
              </IonCardContent>
            </IonCard>
          </IonCol>
        );
      })}
    </IonRow>
  );
};

const FeelingsList: React.FC = observer(() => {
  const { config } = useMst();
  const data = config.data!;

  return (
    <BasePage title="Feelings">
      <IonGrid>
        <IonRow>
          <IonCol>
            <p>
              We tend to experience pleasant feelings when our needs are being
              met.
            </p>
          </IonCol>
        </IonRow>

        <FeelingsBlock feelings={data.pleasantFeelings} />

        <IonRow>
          <IonCol>
            <p>
              We tend to experience unpleasant feelings when our needs are not
              being met.
            </p>
          </IonCol>
        </IonRow>

        <FeelingsBlock feelings={data.unpleasantFeelings} />
      </IonGrid>
    </BasePage>
  );
});

const FeelingsPage: React.FC = () => {
  return (
    <Switch>
      <Route exact path="/feelings/:word">
        <DefinitionPage />
      </Route>
      <Route path="/feelings">
        <FeelingsList />
      </Route>
    </Switch>
  );
};

export default FeelingsPage;
