import React from "react";
import { useParams } from "react-router-dom";
import DefinitionView from "../components/DefinitionView";
import { getFeelingDefinition } from "../models/FeelingDefinitions";

const FeelingDefinition: React.FC = () => {
  const { word } = useParams<{ word: string }>();
  return (
    <DefinitionView
      word={word}
      definition={getFeelingDefinition(word)}
      backLink="/feelings"
    />
  );
};

export default FeelingDefinition;
