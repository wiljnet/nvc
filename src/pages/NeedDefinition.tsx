import React from "react";
import { useParams } from "react-router-dom";
import DefinitionView from "../components/DefinitionView";
import { getNeedDefinition } from "../models/NeedDefinitions";

const NeedDefinition: React.FC = () => {
  const { word } = useParams<{ word: string }>();
  return (
    <DefinitionView
      word={word}
      definition={getNeedDefinition(word)}
      backLink="/needs"
    />
  );
};

export default NeedDefinition;