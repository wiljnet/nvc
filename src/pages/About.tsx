import {
  IonCol,
  IonGrid,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonRow,
  IonText,
} from "@ionic/react";
import React, { useMemo } from "react";
import BasePage from "../components/BasePage";
import "./About.css";

const links = [
  `https://johnkinyon.com/`,
  `https://www.cnvc.org/`,
  `https://www.nonviolentcommunication.com/`,
  `https://baynvc.org/`,
  `https://www.connectionfirst.org/`,
];
const AboutPage: React.FC = () => {
  return (
    <BasePage title="About">
      <IonGrid>
        <IonRow>
          <IonCol>
            <IonText>
              This application is based on{" "}
              <a href="/johnkinyons_feelings_needs_sheet_handout.pdf">
                John Kinyon's Feelings and Needs Inventory
              </a>
            </IonText>
            <hr/>
            <IonText>For more information, see:</IonText>
          </IonCol>
        </IonRow>
        {links.map((link) => (
          <IonRow key={link}>
            <IonCol>
              <IonText>
                <a href={link} target="_blank">{link}</a>
              </IonText>
            </IonCol>
          </IonRow>
        ))}
      </IonGrid>
    </BasePage>
  );
};

export default AboutPage;
