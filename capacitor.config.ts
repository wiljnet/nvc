import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'wilj-nvc-app',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
